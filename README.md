# Sweeney 365

- [ ] Write setup instructions

### Quick-install for iOS dev
1. Clone Repo
2. npm install
3. Open xCode and run

### Android
1. generate apk in fastlane dir
2. adb install android/app/build/outputs/apk/app-release.apk from root dir

#### React Native Flat App Theme v5.2.0

Relevant links:

-   [Documentation](http://strapmobile.com/docs/react-native-flat-app-theme/master/)
-   [Product Page](http://strapmobile.com/react-native-flat-app-theme/)
-	  [Change Log](http://gitstrap.com/strapmobile/FlatApp/blob/v5.2.0/ChangeLog.md)

Check `FAQs` section in docs for queries.
