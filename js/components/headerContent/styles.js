const React = require( "react-native" )

const { StyleSheet, Dimensions, Platform } = React

module.exports = StyleSheet.create( {
  header: {
    flex: 1,
    width: Dimensions.get( "window" ).width,
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 5,
    marginLeft: ( Platform.OS === "ios" ) ? undefined : -30,
  },
  gridHeader: {
    paddingTop: Platform.OS === "android" ? 5 : 0,
  },
  colHeader: {
    marginTop: ( Platform.OS === "android" ) ? 9 : 7,
  },
  btnHeader: {
    marginTop: ( Platform.OS === "android" ) ? -10 : 0,
  },
  leftBtnHeader: {
    alignSelf: "flex-start",
  },
  rightBtnHeader: {
    alignSelf: "flex-end",
  },
  imageHeader: {
    height: 36,
    width: 52,
    resizeMode: "contain",
    alignSelf: "center",
    marginTop: ( Platform.OS === "android" ) ? 0 : 5,
  },
  icon: {
    fontSize: 26,
    color: "#fff",
  },
  signOut: {
    transform: [ { rotate: "180deg" } ],
  },
} )
