import React, { Component } from "react"
import { Image } from "react-native"
import { connect } from "react-redux"
import { actions } from "react-native-navigation-redux-helpers"
import { View, Button } from "native-base"
import Icon from "react-native-vector-icons/FontAwesome"

import { Grid, Col } from "react-native-easy-grid"

import { openDrawer, closeDrawer } from "../../actions/drawer"
import { toggleAllLoginActionsAuth } from "../../actions/auth"
import styles from "./styles"

const {
  reset,
  popRoute,
} = actions

const headerLogo = require( "../../../images/Header-Logo.png" )

class HeaderContent extends Component {

  static propTypes = {
    hideBack: React.PropTypes.bool,
    showMenu: React.PropTypes.bool,
    reset: React.PropTypes.func,
    resetAuth: React.PropTypes.func,
    popRoute: React.PropTypes.func,
    openDrawer: React.PropTypes.func,
    drawerState: React.PropTypes.string,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
    } ),
  }

  logout() {
    // TODO Maybe use replaceAt for logout button
    this.props.reset( this.props.navigation.key )
    this.props.resetAuth()
  }

  popRoute() {
    this.props.popRoute( this.props.navigation.key )
  }

  toggleDrawerOpen() {
    if ( this.props.drawerState === "opened" ) {
      this.props.closeDrawer()
    }
    else {
      this.props.openDrawer()
    }
  }

  render() {
    return (
      <View style={ styles.header }>
        <Grid style={ styles.gridHeader }>
          <Col style={ styles.colHeader }>
            { this.props.hideBack !== true &&
              <Button
                transparent
                style={ [ styles.btnHeader, styles.leftBtnHeader ] }
                onPress={ () => this.popRoute() }
              >
                <Icon name="chevron-left" style={ styles.icon } />
              </Button>
            }
            { false && this.props.showLogout === true &&
              <Button
                transparent
                style={ [ styles.btnHeader, styles.leftBtnHeader ] }
                onPress={ () => this.logout() }
              >
                <Icon name="sign-out" style={ [ styles.icon, styles.signOut ] } />
              </Button>
            }
          </Col>
          <Col>
            <Image source={ headerLogo } style={ styles.imageHeader } />
          </Col>
          <Col style={ styles.colHeader }>
            { this.props.showMenu !== false &&
            <Button
              transparent
              style={ [ styles.btnHeader, styles.rightBtnHeader ] }
              onPress={ () => this.toggleDrawerOpen() }
            >
              <Icon name="bars" style={ styles.icon } />
            </Button>
          }
          </Col>
        </Grid>
      </View>
    )
  }
}

function bindAction( dispatch ) {
  return {
    openDrawer: () => dispatch( openDrawer() ),
    closeDrawer: () => dispatch( closeDrawer() ),
    reset: key => dispatch( reset( [ { key: "login" } ], key, 0 ) ),
    resetAuth: () => dispatch( toggleAllLoginActionsAuth() ),
    popRoute: key => dispatch( popRoute( key ) ),
  }
}

const mapStateToProps = state => ( {
  navigation: state.cardNavigation,
  drawerState: state.drawer.drawerState,
} )

export default connect( mapStateToProps, bindAction )( HeaderContent )
