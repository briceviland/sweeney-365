const React = require( "react-native" )

const { StyleSheet, Dimensions, Platform } = React

const primary = require( "../../themes/variable" ).brandPrimary
const SCREEN = Dimensions.get( "window" )

module.exports = StyleSheet.create( {
  background: {
    flex: 1,
    width: null,
    height: null,
    backgroundColor: primary,
  },
  drawerContent: {
    paddingTop: Platform.OS === "android" ? 30 : 40,
    // marginBottom:(Platform.OS === 'ios') ? -50 : -10
  },
  links: {
    paddingVertical: Platform.OS === "android" ? 12 : 14,
    paddingLeft: Platform.OS === "android" ? 0 : 10,
    borderBottomWidth: Platform.OS === "android" ? 0 : 0,
    borderBottomColor: "transparent",
  },
  newsIcon: {
    width: 30,
    height: 30,
  },
  linkText: {
    paddingLeft: 20,
    fontWeight: "900",
    fontSize: 14,
    lineHeight: Platform.OS === "android" ? 25 : 30,
  },
  weatherListItem: {
    paddingLeft: 0,
  },
  weather: {
    paddingBottom: 6,
    flexDirection: "row",
    alignItems: "center",
  },
  weatherIcon: {
    width: 60,
    height: 60,
    marginRight: 10,
  },
  locationText: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "900",
  },
  weatherText: {
    color: "#fff",
  },
  logoutContainer: {
    padding: 30,
  },
  logoutbtn: {
    paddingTop: 30,
    flexDirection: "row",
    borderTopWidth: 1,
    borderTopColor: "rgba(255, 255, 255, 0.25)",
  },
  profileIcon: {
    width: 40,
    height: 40,
    marginRight: 10,
    marginTop: Platform.OS === "android" ? 6 : 0,
  },
  profileText: {
    fontWeight: "bold",
    color: "#fff",
  },
  profileName: {
    fontSize: 13,
    color: "#fff",
  },
} )
