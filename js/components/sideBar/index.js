import React, { Component } from "react"
import { Image, TouchableOpacity, View } from "react-native"
import { connect } from "react-redux"

import { closeDrawer } from "../../actions/drawer"
import { actions } from "react-native-navigation-redux-helpers"

import { Container, Content, Text, Icon, List, ListItem, Thumbnail } from "native-base"
import { Grid, Col, Row } from "react-native-easy-grid"

import navigateTo from "../../actions/sideBarNav"
import { getCurrentWeather } from "../../actions/weather"

import styles from "./styles"

const {
  reset,
} = actions

const sidebarBackground = require( "../../../images/background-sideBar.png" )

const WEATHER_ICONS = {
  degree: require( "../../../images/weather/degree.png" ),

  "clear-day": require( "../../../images/weather/sunny.png" ),
  "clear-night": require( "../../../images/weather/clear-night.png" ),
  rain: require( "../../../images/weather/rain.png" ),
  snow: require( "../../../images/weather/snow.png" ),
  sleet: require( "../../../images/weather/sleet.png" ),
  wind: require( "../../../images/weather/wind.png" ),
  fog: require( "../../../images/weather/fog.png" ),
  cloudy: require( "../../../images/weather/cloudy.png" ),
  "partly-cloudy-day": require( "../../../images/weather/partly-cloudy.png" ),
  "partly-cloudy-night": require( "../../../images/weather/cloudy-night.png" ),
}

const MENU_ICONS = {
  cabin: require( "../../../images/icons/icon-cabin.png" ),
  camera: require( "../../../images/icons/icon-camera.png" ),
  user: require( "../../../images/icons/icon-user.png" ),
  calendar: require( "../../../images/icons/icon-calendar.png" ),
  news: require( "../../../images/icons/icon-news.png" ),
  video_camera: require( "../../../images/icons/icon-video-camera.png" ),
}

// TODO Remove reset/auth navigation related items

class SideBar extends Component {

  static propTypes = {
    currently: React.PropTypes.object,
    getCurrentWeather: React.PropTypes.func,
    reset: React.PropTypes.func,
    navigateTo: React.PropTypes.func,
    closeDrawer: React.PropTypes.func,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
    } ),
    user_display_name: React.PropTypes.string,
  }

  constructor() {
    super()
    this.state = {
      loadingWeather: true,
      currently: new Object(),
    }
  }

  componentDidMount() {
    if ( !_.isEmpty( this.props.currently ) ) { this.setState( { loadingWeather: false, currently: this.props.currently } ) }

    const HOUR = 60 * 60 * 1000
    if ( _.isEmpty( this.props.currently ) || Date.now() - this.props.currently.updated > HOUR ) {
      this.props.getCurrentWeather().then( ( currently ) => {
        if ( currently ) {
          this.setState( { loadingWeather: false, currently } )
        }
      } )
    }
  }

  navigateTo( route ) {
    this.props.navigateTo( route, "home" )
  }

  reset() {
    this.props.reset( this.props.navigation.key )
  }

  render() {
    return (
      <Container>
        <Image source={ sidebarBackground } style={ styles.background }>
          <Content style={ styles.drawerContent } contentContainerStyle={ { justifyContent: "space-between", flex: 1 } } scrollEnabled={ false }>
            <List foregroundColor={ "white" }>
              { !this.state.loadingWeather &&
              <ListItem header first style={ [ styles.links, styles.weatherListItem ] }>
                <View style={ styles.weather }>
                  <Image source={ WEATHER_ICONS[ this.state.currently.icon ] } style={ styles.weatherIcon } />
                  <View>
                    <TouchableOpacity onPress={ () => this.navigateTo( "home" ) }>
                      <Text style={ styles.locationText }>Camp Sweeney</Text>
                      <Text style={ styles.weatherText }>{this.state.currently.summary}, {Math.round( this.state.currently.temperature )}°</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </ListItem>
            }
              <ListItem button onPress={ () => this.navigateTo( "cabins" ) } style={ styles.links } >
                <Image source={ MENU_ICONS[ "cabin" ] } style={ styles.newsIcon } />
                <Text style={ styles.linkText }> CABINS</Text>
              </ListItem>
              <ListItem button onPress={ () => this.navigateTo( "news" ) } style={ styles.links } >
                <Image source={ MENU_ICONS[ "news" ] } style={ styles.newsIcon } />
                <Text style={ styles.linkText } >NEWS</Text>
              </ListItem>
              <ListItem button onPress={ () => this.navigateTo( "livestream" ) } style={ styles.links } >
                <Image source={ MENU_ICONS[ "video_camera" ] } style={ styles.newsIcon } />
                <Text style={ styles.linkText } >CAMP VIDEOS</Text>
              </ListItem>
              <ListItem button onPress={ () => this.navigateTo( "smugmug" ) } style={ styles.links } >
                <Image source={ MENU_ICONS[ "camera" ] } style={ styles.newsIcon } />
                <Text style={ styles.linkText } >CAMP PHOTOS</Text>
              </ListItem>
            </List>
            <View style={ styles.logoutContainer }>
              <View style={ styles.logoutbtn } foregroundColor={ "white" }>
                <Grid>
                  <Col>
                    <TouchableOpacity onPress={ () => this.navigateTo( "profile" ) } style={ { alignSelf: "flex-start", flexDirection: "row" } }>
                      <Image source={ MENU_ICONS[ "user" ] } style={ styles.profileIcon } />
                      <View>
                        <Text style={ styles.profileText }>PROFILE</Text>
                        <Text style={ styles.profileName }>{ this.props.user_display_name }</Text>
                      </View>
                    </TouchableOpacity>
                  </Col>
                </Grid>
              </View>
            </View>
          </Content>
        </Image>
      </Container>
    ) // : return
  } // : render()
}

function bindAction( dispatch ) {
  return {
    getCurrentWeather: () => dispatch( getCurrentWeather() ),
    navigateTo: ( route, homeRoute ) => dispatch( navigateTo( route, homeRoute ) ),
    reset: key => dispatch( closeDrawer(), reset( [ { key: "login" } ], key, 0 ) ),
  }
}

const mapStateToProps = state => ( {
  currently: state.weather.currently,
  navigation: state.cardNavigation,
  user_display_name: state.auth.user_data.user_display_name,
} )

export default connect( mapStateToProps, bindAction )( SideBar )
