import React from "react"
import { StyleSheet } from "react-native"
import { View, Text } from "native-base"

const PostTitle = props => (
  <View style={ styles.postInfo }>
    <Text numberOfLines={ 1 } style={ styles.postTitle }>
      { props.title }
    </Text>
  </View>
)

const styles = StyleSheet.create( {
  postInfo: {
    flexDirection: "column",
    padding: 15,
    flex: 1,
  },
  postTitle: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 18,
    marginBottom: 2,
  },
} )

export default PostTitle
