import moment from "moment"
import React from "react"
import { StyleSheet } from "react-native"
import { View, Text } from "native-base"
import Icon from "react-native-vector-icons/FontAwesome"

const PostTime = props => (
  <View style={ styles.postTime }>
    <Icon name="clock-o" style={ styles.postTimeIcon } />
    <Text style={ styles.postTimeText }>{ moment( props.time ).fromNow() }</Text>
  </View>
)

const styles = StyleSheet.create({
  postTime: {
    position: "absolute",
    top: 0,
    right: 0,
    flex: 1,
    zIndex: 9,
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 2,
    paddingHorizontal: 6,
    backgroundColor: "rgba(0, 0, 0, 0.55)",
  },
  postTimeText: {
    fontSize: 13,
    marginLeft: 4,
  },
  postTimeIcon: {
    color: "#fff",
  },
})

export default PostTime
