import React from "react"
import { WebView, Dimensions, StyleSheet } from "react-native"

const SCREEN = Dimensions.get( "window" )

const PostVideo = props => (
  props.uri ?
  <WebView
    style={ styles.video }
    source={ { uri: props.uri } }
    startInLoadingState={ true }
  />
  :
  null
)

const styles = StyleSheet.create( {
  video: {
    height: ( SCREEN.width - 30 ) * 9 / 16,
  },
} )

export default PostVideo
