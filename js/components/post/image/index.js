import React from "react"
import { Image, StyleSheet } from "react-native"

const PostImage = props => (
  props.uri ?
  <Image style={ styles.postImage } source={ { uri: props.uri } } />
  :
  null
)

const styles = StyleSheet.create( {
  postImage: {
    height: 200,
  },
} )

export default PostImage
