import React, { Component } from "react"
import { StyleSheet, TouchableOpacity } from "react-native"
import { View } from "native-base"

import PostTime from "./time"
import PostTags from "./tags"
import PostImage from "./image"
import PostVideo from "./video"
import PostTitle from "./title"
import PostExcerpt from "./excerpt"

const Post = props => {

  const { onPress, backgroundColor, activeOpacity, children } = props

  return (
    <TouchableOpacity onPress={ onPress } activeOpacity={ activeOpacity }>
      <View style={ [ styles.postContainer, { backgroundColor: backgroundColor } ] } >
        { children }
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create( {
  postContainer: {
    flex: 1,
    flexDirection: "column",
    margin: 7.5,
    shadowRadius: 4,
    shadowColor: "#000",
    shadowOpacity: 0.25,
    shadowOffset: {
      width: 0,
      height: 0,
    },
  },
} )

module.exports = {
  Post: Post,
  Tags: PostTags,
  Time: PostTime,
  Image: PostImage,
  Video: PostVideo,
  Title: PostTitle,
  Excerpt: PostExcerpt,
}

export default Post
