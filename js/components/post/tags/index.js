import React from "react"
import { StyleSheet } from "react-native"
import { View, Thumbnail } from "native-base"

const PostTags = props => {

  const { campers, tags } = props

  const thumbs = _.map( campers, ( camper, index ) => (
    tags.indexOf( camper.id.toString() ) > -1 &&
    <Thumbnail
      key={ index }
      source={ { uri: camper.photo_url } }
      style={ styles.camperTagImage }
    />
  ) )

  return ( thumbs.length > 0 &&
    <View style={ styles.camperTags }>
      { thumbs }
    </View>
  )

}

const styles = StyleSheet.create( {
  camperTags: {
    position: 'absolute',
    top: 0,
    left: 0,
    padding: 4,
    flex: 1,
    flexDirection: "row",
    zIndex: 9,
  },
  camperTagImage: {
    borderWidth: 2,
    borderColor: "#fff",
    margin: 2,
  },
} )

export default PostTags
