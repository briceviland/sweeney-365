import React from "react"
import { StyleSheet } from "react-native"
import { Text } from "native-base"

const PostExcerpt = props => (
  props.excerpt ?
  <Text numberOfLines={ 2 } style={ styles.postExcerpt }>
    { props.excerpt }
  </Text>
  :
  null
)

const styles = StyleSheet.create( {
  postExcerpt: {
    color: "#fff",
    paddingHorizontal: 15,
    paddingBottom: 15,
    marginTop: -10,
  },
} )

export default PostExcerpt
