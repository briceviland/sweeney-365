import type { Action } from "./types"

export const ADD_NEWS_FEED_POST = "ADD_NEWS_FEED_POST"
export const RESET_NEWS_FEED = "RESET_NEWS_FEED"

export function addNewsFeedItem( post:Object ):Action {
  return {
    type: ADD_NEWS_FEED_POST,
    post,
  }
}

export function resetNewsFeed():Action {
  return {
    type: RESET_NEWS_FEED,
  }
}

export function actGetNewsFeedPosts() {
  const site_url = "https://www.campsweeney.org/wp-json/dsm/v1"
  const news_url = `${ site_url }/news`
  // TODO Only get posts modified since most recent post date
  return ( dispatch, getState ) => fetch( news_url, {
    page: 1,
  } )
    .then( response => response.json() )
    .then( ( json ) => {
      if ( json.posts && json.posts.length > 0 ) {
        const posts = []
        json.posts.forEach( ( news_post ) => {
          const _news_post = {
            ...news_post,
            last_updated: Date.now(),
          }
          dispatch( addNewsFeedItem( _news_post ) )
          posts.push( _news_post )
        } )
        return posts
      }
        // : TODO Handle error
    } )
}
