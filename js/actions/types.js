// : TODO enforce Object attributes
export type Action =
  { type: 'PUSH_NEW_ROUTE', route: string, passProps:any }
    | { type: 'POP_ROUTE', passProps:any }
    | { type: 'POP_TO_ROUTE', route: string, passProps:any }
    | { type: 'REPLACE_ROUTE', route: string, passProps:any }
    | { type: 'REPLACE_OR_PUSH_ROUTE', route: string, passProps:any }
    | { type: 'RESET_ROUTES' }
    | { type: 'OPEN_DRAWER'}
    | { type: 'CLOSE_DRAWER'}
    | { type: 'TOGGLE_HAS_SHOWN_SETUP', hasShownSetupScreen: bool }
    | { type: 'TOGGLE_LOGIN_AUTH', loggedIn: bool }
    | { type: 'SET_JTW_TOKEN_AUTH', jwtToken: string }
    | { type: 'SET_USER_DATA_AUTH', user_data: Object }
    | { type: 'SET_CAMPER_DATA_AUTH', camper: Object }
    | { type: 'ADD_ALBUM_SMUG', album: Object }
    | { type: 'SET_CURRENT_WEATHER', currently: Object }
    | { type: 'ADD_LIVE_STREAM_VIDEO', currently: Object }
    | { type: 'ADD_LIVE_STREAM_CURRENT_VIDEO', currently: string }
    | { type: 'ADD_NEWS_FEED_ITEM', currently: Object }
    | { type: 'RESET_NEWS_FEED', currently: Object }
    | { type: 'ADD_CABIN_INFO', cabin: Object }
    | { type: 'SET_NOTIFICATION_CHANNELS', channels: Array }
    | { type: 'POSTS_NEED_REFRESH', postsNeedRefresh: bool }
    | { type: 'ADD_CAMPER_OF', camper: Object }
    | { type: 'RESET_CAMPER_OF' }

export type Dispatch = ( action:Action | Array<Action> ) => any;
export type GetState = () => Object;
export type PromiseAction = Promise<Action>;
