import type { Action } from "./types"

export const ADD_LIVE_STREAM_VIDEO = "ADD_LIVE_STREAM_VIDEO"
export const ADD_LIVE_STREAM_CURRENT_VIDEO = "ADD_LIVE_STREAM_CURRENT_VIDEO"

export function addLiveStreamVideo( video:Object ):Action {
  return {
    type: ADD_LIVE_STREAM_VIDEO,
    video,
  }
}

export function setLiveStreamCurrentVideo( videoUrl:string ):Action {
  return {
    type: ADD_LIVE_STREAM_CURRENT_VIDEO,
    videoUrl,
  }
}

export function actGetLiveStreamVideos() {
  const site_url = "https://www.campsweeney.org/wp-json/dsm/v1"
  const live_stream_url = `${ site_url }/livestream`

  return ( dispatch, getState ) => fetch( live_stream_url )
    .then( response => response.json() )
    .then( ( json ) => {
      const videos = new Array()
      json.forEach( ( video ) => {
        const _video = {
          ...video,
          last_updated: Date.now(),
        }
        videos.push( _video )
        dispatch( addLiveStreamVideo( _video ) )
      } )
      return videos
    } )
}

export function actGetLiveStreamCurrentVideo() {
  //: TODO implement actGetLiveStreamCurrentVideo
}

export function actSetLiveStreamCurrentVideo( videoUrl ) {
  return ( dispatch, getState ) => {
    dispatch( setLiveStreamCurrentVideo( videoUrl ) )
  }
}
