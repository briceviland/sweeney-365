import type { Action } from "./types"

export const TOGGLE_HAS_SHOWN_SETUP = "TOGGLE_HAS_SHOWN_SETUP"
export const TOGGLE_LOGIN_AUTH = "TOGGLE_LOGIN_AUTH"
export const SET_JTW_TOKEN_AUTH = "SET_JTW_TOKEN_AUTH"
export const SET_USER_DATA_AUTH = "SET_USER_DATA_AUTH"
export const SET_CAMPER_DATA_AUTH = "SET_CAMPER_DATA_AUTH"

export function toggleHasShownSetup( hasShownSetupScreen:bool ):Action {
  return {
    type: TOGGLE_HAS_SHOWN_SETUP,
    hasShownSetupScreen,
  }
}

export function toggleLoginAuth( loggedIn:bool ):Action {
  return {
    type: TOGGLE_LOGIN_AUTH,
    loggedIn,
  }
}

export function setJWTTokenAuth( jwtToken:string ):Action {
  // : TODO fix action name typo
  return {
    type: SET_JTW_TOKEN_AUTH,
    jwtToken,
  }
}

export function setUserDataAuth( user_data:Object ):Action {
  return {
    type: SET_USER_DATA_AUTH,
    user_data,
  }
}

export function setCamperDataAuth( camper:Object ):Action {
  return {
    type: SET_CAMPER_DATA_AUTH,
    camper,
  }
}

export function toggleAllLoginActionsAuth():Action {
  return ( dispatch ) => {
    dispatch( setJWTTokenAuth( "" ) )
    dispatch( toggleLoginAuth( false ) )
    dispatch( setUserDataAuth( {} ) )
    dispatch( setCamperDataAuth( null ) )
  }
}

export function actLoginAuth( username, password ) {
  const site_url = "https://www.campsweeney.org/wp-json/"
  const auth_url = `${ site_url }jwt-auth/v1/token/`

  const authPromises = []

  return ( dispatch, getState ) => fetch( auth_url, {
    method: "POST",
    body: JSON.stringify( {
      username,
      password,
    } ),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  } )
    .then( response => response.json() )
    .then( ( json ) => {
      // : TODO Check for valid `camper_id`
      if ( json.token && json.camper_id && json.camper_cabin ) {
        const user_data = {
          user_email: json.user_email,
          user_display_name: json.user_display_name,
        }

        authPromises.push( dispatch( setJWTTokenAuth( json.token ) ) )
        authPromises.push( dispatch( toggleLoginAuth( true ) ) )
        authPromises.push( dispatch( setUserDataAuth( user_data ) ) )

        const camper_ids = json.camper_id.split( "," )
        const camper_cabins = json.camper_cabin.split( "," )
        const campers = _.zip( camper_ids, camper_cabins )
        _.each( campers, ( camper ) => {
          const id = camper[ 0 ]
          const cabin = camper[ 1 ]
          authPromises.push( dispatch( actGetCamperInfoAuth( id.trim(), cabin.trim() ) ) )
        } )
        Promise.all( authPromises )
        return true
      }
        // : TODO Add login failed indication
      return Promise.resolve( "Error" )
      // return false
    } )
}

export function actGetCamperInfoAuth( camper_id, camper_cabin ) {

  const url = `https://tango.campsweeney.org/tango/classinq3br.taf?_function=json&Master_uid1=${ camper_id }`

  return dispatch => fetch( url )
    .then( response => response.json() )
    .then( ( json ) => {
      if ( json.id ) {
        const camper_photo_url = `https://tango.campsweeney.org/dbphotos/${ camper_id }.jpg`
        const camper = {
          id: json.id,
          name: `${ json.first_name } ${ json.last_name }`,
          first_name: json.first_name,
          last_name: json.last_name,
          cabin: camper_cabin,
          photo_url: camper_photo_url,
          schedule: _.map(json.classes, v => ( v ) )
        }
        dispatch( setCamperDataAuth( camper ) )
      }
    } )
}

export function actValidateJWTTokenAuth() {
  // : TODO Add util functions for API urls

  const site_url = "https://www.campsweeney.org/wp-json/"
  const auth_url = `${ site_url }jwt-auth/v1/token/validate`

  return ( dispatch, getStore ) => {
    const jwtToken = getStore().auth.jwtToken

    return fetch( auth_url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${ jwtToken }`,
      },
    } )
    .then( response => response.json() )
    .then( ( json ) => {
      if ( json.code == "jwt_auth_valid_token" && json.data.status == 200 ) {
        return true
      }
      dispatch( toggleAllLoginActionsAuth() )
      return false
    } )
  }
}


export function actInviteUser( firstName, lastName, email ) {

  const urlBase = "https://www.campsweeney.org/wp-json/dsm/v1"
  const userUrl = `${ urlBase }/invite_user`

  return ( dispatch, getStore ) => {
    const store = getStore()
    const jwtToken = store.auth.jwtToken
    const camperIds = store.auth.campers.map( camper => camper.id ).join(', ')
    const camperCabins = store.auth.campers.map( camper => camper.cabin ).join(', ')

    const body = {
      first_name: firstName,
      last_name: lastName,
      email: email,
      camper_ids: camperIds,
      camper_cabins: camperCabins,
    }

    return fetch( userUrl, {
      method: "POST",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": `Bearer ${ jwtToken }`,
      },
      body: JSON.stringify( body ),
    } )
    .then( response => response.json() )
    .then( ( json ) => {
      return json
    } )
  }
}
