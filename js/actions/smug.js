import _ from "lodash/core"
import type { Action } from "./types"

export const ADD_ALBUM_SMUG = "ADD_ALBUM_SMUG"

const GET_JSON = {
  method: "GET",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
}

export function addAlbumSmug( album:Object ):Action {
  return {
    type: ADD_ALBUM_SMUG,
    album,
  }
}

export function getAlbumsListSmug() {
  const base = "https://api.smugmug.com"
  const albums = "api/v2/user/campsweeney!albums"
  const auth = "?APIKey=2QK5WnQN4f3L5DRqFkd7PBbcBQnfXVGG"

  const albums_url = `${ base }/${ albums }/${ auth }`

  return ( dispatch, getStore ) => fetch( albums_url, GET_JSON )
    .then( response => response.json() )
    .then( ( json ) => {
      if ( json.Code == 200 ) {
        // : TODO Break out below dispatch into function
        const albumsPromises = new Array()
        json.Response.Album.forEach( ( album ) => {
          const uri = album.Uris.AlbumHighlightImage.Uri
          const image_uri = `${ base }/${ uri }/${ auth }`
          const albumPromise = dispatch( () => fetch( image_uri, GET_JSON )
            .then( response => response.json() )
            .then( ( json ) => {
              if ( json.Code == 200 ) {
                const image = json.Response.AlbumImage
                const smug_album = {
                  title: album.Title,
                  key: album.AlbumKey,
                  date: album.Date,
                  last_updated: album.LastUpdated,
                  thumbnail: image.ThumbnailUrl,
                  archived_uri: image.ArchivedUri,
                }
                dispatch( addAlbumSmug( smug_album ) )
                // : TODO Add check for updated date
                return smug_album
              }
                // : TODO Error handling
            } ) ) // : dispatch
          albumsPromises.push( albumPromise )
        } ) // : forEach()
        return Promise.all( albumsPromises )
      }
        // : TODO Error handling
      return Promise.resolve()
    } ) // : return
}

export function getAlbumImagesSmug( album_key, count, page, total ) {
  const base = "https://api.smugmug.com"
  const images = `api/v2/album/${ album_key }!images`
  const auth = '?count=' + count + '&start=' + page + '&APIKey=2QK5WnQN4f3L5DRqFkd7PBbcBQnfXVGG';
  const images_url = `${ base }/${ images }/${ auth }`

  return ( dispatch, getStore ) => fetch( images_url, GET_JSON )
    .then( response => response.json() )
    .then( ( json ) => {
      if ( json.Code == 200 ) {
        const images = new Array()
        const data = new Array();
        console.log(json.Response)
        if(total !== true){
          json.Response.AlbumImage.forEach( ( image ) => {
            images.push( {
              key: image.ImageKey,
              caption: image.Caption,
              thumb: image.ThumbnailUrl,
              // photo: image.ThumbnailUrl.replace('/Th/', '/L/').replace('-Th.jpg', '-L.jpg')
              photo: image.ThumbnailUrl.replace( "/Th/", "/M/" ).replace( "-Th.jpg", "-M.jpg" ),
            } )
          } )
          return images;
        }
        else if(count !== undefined){
          json.Response.AlbumImage.forEach( ( image ) => {
            images.push( {
              key: image.ImageKey,
              caption: image.Caption,
              thumb: image.ThumbnailUrl,
              // photo: image.ThumbnailUrl.replace('/Th/', '/L/').replace('-Th.jpg', '-L.jpg')
              photo: image.ThumbnailUrl.replace( "/Th/", "/M/" ).replace( "-Th.jpg", "-M.jpg" ),
            } )
          } )
          if(page * count >= json.Response.Pages.Total + count){
            data = {
              images: new Array(),
              total: json.Response.Pages.Total
            }
          }
          else{
            data = {
              images: images,
              total: json.Response.Pages.Total
            }
          }
          return data;
        }
        return true;
      }
        // : TODO Error handling
      return false
    } ) // : return
}
