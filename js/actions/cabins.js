import type { Action } from "./types"

export const ADD_CABIN_INFO = "ADD_CABIN_INFO"
export const ADD_CAMPER_OF = "ADD_CAMPER_OF"
export const RESET_CAMPER_OF = "RESET_CAMPER_OF"

export function addCabinInfo( cabin:Object ):Action {
  return {
    type: ADD_CABIN_INFO,
    cabin,
  }
}

export function addCamperOf( camper:Object ):Action {
  return {
    type: ADD_CAMPER_OF,
    camper,
  }
}

export function resetCamperOf():Action {
  return {
    type: RESET_CAMPER_OF,
  }
}

export function actGetCabinsInfo() {
  const siteUrl = "https://www.campsweeney.org/wp-json/dsm/v1"
  const cabinsUrl = `${ siteUrl }/cabins`

  return dispatch => fetch( cabinsUrl )
    .then( response => response.json() )
    .then( ( json ) => {
      const cabins = []
      json.forEach( ( cabin ) => {
        const _cabin = {
          ...cabin,
          last_updated: Date.now(),
        }
        cabins.push( _cabin )
        dispatch( addCabinInfo( _cabin ) )

        const camper_of_the_day = _cabin.camper_of_the_day
        const camper_of_the_week = _cabin.camper_of_the_week

        if ( camper_of_the_day !== false ) {
          if ( parseInt( camper_of_the_day, 10 ) === camper_of_the_day ) {
            dispatch( actGetCamperOfData( camper_of_the_day, _cabin.name, 'day' ) )
          }
        } else {
          dispatch( resetCamperOf() )
        }

        if ( camper_of_the_week !== false ) {
          if ( parseInt( camper_of_the_week, 10 ) === camper_of_the_week ) {
            dispatch( actGetCamperOfData( camper_of_the_week, _cabin.name, 'week' ) )
          }
        } else {
          dispatch( resetCamperOf() )
        }
      } )
      return cabins
    } )
}

export function actGetCamperOfData( camper_id, camper_cabin, type ) {

  const url = `https://tango.campsweeney.org/tango/classinq3br.taf?_function=json&Master_uid1=${ camper_id }`

  return (dispatch) => fetch( url )
    .then( response => response.json() )
    .then( ( json ) => {
      if ( json.id ) {
        const camper_photo_url = `https://tango.campsweeney.org/dbphotos/${ camper_id }.jpg`
        const camper = {
          key: `${camper_cabin}-${type}`,
          cabin: camper_cabin,
          type: type,
          id: json.id,
          name: `${ json.first_name } ${ json.last_name }`,
          photo_url: camper_photo_url,
        }

        dispatch( addCamperOf( camper ) )
      }
    } )
}
