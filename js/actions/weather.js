import type { Action } from "./types"

export const SET_CURRENT_WEATHER = "SET_CURRENT_WEATHER"

export function setCurrentWeather( currently:Object ):Action {
  return {
    type: SET_CURRENT_WEATHER,
    currently,
  }
}

export function getCurrentWeather() {
  const api_key = "fef49726318c909f8a20cca1f6f7f837"
  const lat = "33.687665"
  const lng = "-97.004652"
  const url = `https://api.darksky.net/forecast/${ api_key }/${ lat },${ lng }`

  return ( dispatch, getState ) => fetch( url )
    .then( response => response.json() )
    .then( ( json ) => {
      const currently = {
        summary: json.currently.summary,
        temperature: json.currently.temperature,
        humidity: json.currently.humidity,
        windSpeed: json.currently.windSpeed,
        icon: json.currently.icon,
        last_updated: Date.now(),
      }
      dispatch( setCurrentWeather( currently ) )
      return currently
    } )
}
