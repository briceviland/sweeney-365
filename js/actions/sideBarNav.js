import { NavigationExperimental } from "react-native"
import { actions } from "react-native-navigation-redux-helpers"
import { closeDrawer } from "./drawer"
import { toggleAllLoginActionsAuth } from "./auth"

const {
  StateUtils: NavigationStateUtils,
} = NavigationExperimental

const {
  replaceAt,
  popRoute,
  pushRoute,
  reset,
  jumpTo,
} = actions

export default function navigateTo( route, homeRoute ) {
  return ( dispatch, getState ) => {
    const navigation = getState().cardNavigation
    const currentRouteKey = navigation.routes[ navigation.routes.length - 1 ].key
    dispatch( closeDrawer() )
    if ( homeRoute === "home" && route === "login" ) {
      dispatch( reset( [ { key: "login" } ], navigation.key, 0 ) )
      dispatch( toggleAllLoginActionsAuth() )
    } else if ( currentRouteKey !== homeRoute && route !== homeRoute ) {
      dispatch( reset( [ { key: "home", index: 0 } ], navigation.key, 0 ) )
      dispatch( pushRoute( { key: route, index: 1 }, navigation.key ) )
    } else if ( currentRouteKey !== homeRoute && route === homeRoute ) {
      dispatch( popRoute( navigation.key ) )
    } else if ( currentRouteKey === homeRoute && route !== homeRoute ) {
      dispatch( pushRoute( { key: route, index: navigation.index + 1 }, navigation.key ) )
    }
    else {
      dispatch( replaceAt( currentRouteKey, { key: route, index: 1 }, navigation.key ) )
    }
  }
}
