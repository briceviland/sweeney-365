import type { Action } from "./types"

export const SET_NOTIFICATION_CHANNELS = "SET_NOTIFICATION_CHANNELS"
export const SET_DEVICE_TOKEN = "SET_DEVICE_TOKEN"
export const POSTS_NEED_REFRESH = "POSTS_NEED_REFRESH"

export function setNotificationChannels( channels:Array ):Action {
  return {
    type: SET_NOTIFICATION_CHANNELS,
    channels,
  }
}

export function setDeviceToken( token:string ):Action {
  return {
    type: SET_DEVICE_TOKEN,
    token,
  }
}

export function actPostsNeedRefresh( postsNeedRefresh:bool ):Action {
  return {
    type: POSTS_NEED_REFRESH,
    postsNeedRefresh,
  }
}

export function registerTokenForUser( deviceToken ) {

  const urlBase = "https://www.campsweeney.org/wp-json/dsm/v1"
  const userUrl = `${ urlBase }/user`

  return ( dispatch, getStore ) => {

    const jwtToken = getStore().auth.jwtToken

    const body = {
      device_token: `private_device_${ deviceToken.toUpperCase() }`,
    }

    return fetch( userUrl, {
      method: "POST",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": `Bearer ${ jwtToken }`,
      },
      body: JSON.stringify( body ),
    } )
    .then( response => response.json() )
    .then( ( json ) => {
      console.log('JSON', json)
      return true
    } )
  }
}
