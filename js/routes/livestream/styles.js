const React = require( "react-native" )

const { StyleSheet, Dimensions, Platform } = React

const SCREEN = Dimensions.get( "window" )
const COLORS = require( "../../themes/variable" )

module.exports = StyleSheet.create( {
  backgroundContainer: {
    backgroundColor: COLORS.darkBackground,
  },
  flipCardBase: {
    borderWidth: 0,
    borderColor: "transparent",
  },
  shadowBase: {
    shadowRadius: 4,
    shadowColor: "#000",
    shadowOpacity: 0.25,
    shadowOffset: {
      width: 0,
      height: 0,
    },
  },
  headingContainer: {
    // flex: 1,
    alignItems: "center",
    // justifyContent: "center",
    padding: 20,
  },
  headingText: {
    fontSize: 21,
    lineHeight: 21,
    marginTop: 2,
    fontWeight: "900",
  },
  subHeadingText: {
    fontSize: 19,
    lineHeight: 19,
  },
  accordion: {
    margin: 7.5,
    backgroundColor: "#fff",
  },
  videosContainer: {
    margin: 7.5,
  },
  videoHeader: {
    marginHorizontal: 7.5,
    marginTop: 15,
    padding: 10,
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    // alignItems: "center",
  },
  videoHeaderTitle: {
    flex: 11,
    fontWeight: "900",
    alignSelf: "flex-start",
    marginBottom: Platform.OS === "android" ? 4 : 0,
  },
  videoHeaderIconContainer: {
    flex: 1,
    justifyContent: "center",
  },
  videoHeaderIcon: {
    fontSize: 25,
    color: "#fff",
    marginBottom: Platform.OS === "android" ? 2 : 0,
  },
  video: {
    height: Platform.OS === "android" ? SCREEN.width * ( 9 / 16 ) : ( SCREEN.width - 30 ) * ( 9 / 16 ),
  },
  videoSquare: {
    margin: 7.5,
    height: ( SCREEN.width - 45 ) / 2 * 9 / 16,
  },
  videoSquareTitle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "green",
    padding: 15,
  },
  streamContainer: {
    margin: 7.5,
    marginBottom: 0,
  },
  streamView: {
    height: ( SCREEN.width - 30 ) * 9 / 16,
  },
  streamThumbnail: {
    flex: 1,
    height: null,
    width: null,
  },
  streamTitle: {
    fontSize: 21,
    fontWeight: "900",
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
} )
