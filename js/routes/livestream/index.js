import _ from "lodash/core"
import React, { Component } from "react"
import { connect } from "react-redux"
import {
  Image,
  WebView,
  Platform,
  Dimensions,
  RefreshControl,
  StatusBar,
  TouchableOpacity,
} from "react-native"
import {
  Container,
  Header,
  Content,
  Text,
  View,
  Spinner,
  Icon
} from "native-base"

import { Grid, Col, Row } from "react-native-easy-grid"
import FlipCard from "react-native-flip-card"
import PixAccordion from "react-native-pixfactory-accordion"

import { actions } from "react-native-navigation-redux-helpers"
import { actGetLiveStreamVideos } from "../../actions/livestream"

import HeaderContent from "../../components/headerContent/"

import theme from "../../themes/base-theme"
import styles from "./styles"

var Orientation = require('react-native-orientation');

const {
  popRoute,
  pushRoute,
} = actions

const COLORS = [ "#01cca1", "#2594f6", "#c83833", "#e86713" ]

const CONTENT_INSET = Platform.OS === "android" ? { top: 0, left: 0, bottom: 0, right: 0 } : { top: 0, left: 0, bottom: 0, right: 0 }

const liveStreamThumbnail = require( "../../../images/thumbnail-livestream.png" )

class LiveStream extends Component {

  static propTypes = {
    livestream_url: React.PropTypes.string,
    videos: React.PropTypes.array,
    popRoute: React.PropTypes.func,
    pushRoute: React.PropTypes.func,
    getVideos: React.PropTypes.func,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
    } ),
  }

  constructor( props ) {
    super( props )
    this.state = {
      archiveLoaded: this.props.videos.length > 0,
      liveStreamLoading: true,
      videos: props.videos,
      isRefreshing: false,
    }
  }

  componentWillMount() {
    Orientation.unlockAllOrientations()

    const interval = setInterval(() => {
      StatusBar.setHidden( false, false )
    }, 500)

    this.setState( { 'statusBarInterval': interval } )

    // if ( this.state.videos.length == 0 ) {
      // TODO fix weirdness in live stream video loading
      // this.refreshVideos()
    setTimeout( this.pullDownRefresh, 200 )
    // }
  }

  componentWillUnmount() {
    Orientation.lockToPortrait()
    clearInterval( this.state.statusBarInterval )
  }

  pullDownRefresh = () => {
    this.setState( { isRefreshing: true } )
    this.refreshVideos()
  }

  refreshVideos = () => {
    this.props.getVideos().then( ( videos ) => {
      this.setState( {
        videos,
        isRefreshing: false,
        archiveLoaded: true,
      } )
    } )
  }

  popRoute() {
    this.props.popRoute( this.props.navigation.key )
  }

  pushRoute( route, passProps ) {
    this.props.pushRoute( { key: route, index: this.props.navigation.index + 1, passProps }, this.props.navigation.key )
  }

  _renderLiveStreamRow = video_url => (
    <Col>
      <View style={ styles.streamContainer }>
        <FlipCard
          style={ { height: 200, borderWidth: 0, borderColor: "transparent" } }
          flipHorizontal
          flipVertical={ false }
          alignHeight
          flip={ !this.state.liveStreamLoading }
          clickable={ this.state.liveStreamLoading }
        >
          <Image source={ liveStreamThumbnail } style={ styles.streamThumbnail } />
          <WebView
            ref={ "currentStreamWebView" }
            style={ styles.streamView }
            source={ { uri: `https:${ video_url }` } }
            contentInset={ { top: -40, left: -40, bottom: -40, right: -40 } }
            onLoadEnd={ () => this.setState( { liveStreamLoading: false } ) }
          />
        </FlipCard>
      </View>
    </Col>
    )

  render() {
    const video_url = this.props.livestream_url
    const videos = this.state.videos

    return (
      <Container theme={ theme } style={ styles.backgroundContainer }>
        <Header>
          <HeaderContent showMenu={ this.props.loggedIn } />
        </Header>
        <Content
          style={ { marginBottom: ( Platform.OS === "ios" ) ? -50 : -10 } }
          refreshControl={
            <RefreshControl
              refreshing={ this.state.isRefreshing }
              onRefresh={ this.pullDownRefresh }
              tintColor={ "#fff" }
            />
          }
        >
          <Grid style={ styles.videosContainer }>
            <View style={ styles.headingContainer }>
              <Text style={ [ styles.headingText, styles.subHeadingText ] }>CAMP VIDEOS</Text>
            </View>
            <Row>
              { this.state.archiveLoaded && video_url !== "" && this._renderLiveStreamRow( video_url ) }
            </Row>
            {
              _.map( videos.slice(0, 10), ( video, index ) => (
                <PixAccordion
                  key={ video.id }
                  style={ styles.accordion }
                  renderHeader={ () => (
                    <View style={ [ styles.videoHeader, { backgroundColor: COLORS[ index % COLORS.length ] } ] }>
                      <Text style={ styles.videoHeaderTitle }>{video.title}</Text>
                      <View style={ styles.videoHeaderIconContainer }>
                        <Icon name="play-circle" style={ styles.videoHeaderIcon } />
                      </View>
                    </View>
                  ) }
                  collapsed
                >
                  <View style={ styles.videoContainer }>
                    <WebView
                      style={ styles.video }
                      source={ { uri: `https://${ video.url }` } }
                      contentInset={ CONTENT_INSET }
                      startInLoadingState={ true }
                    />
                  </View>
                </PixAccordion>
              ) )
            }
          </Grid>
        </Content>
      </Container>
    )
  } // : render()

}

function bindAction( dispatch ) {
  return {
    getVideos: () => dispatch( actGetLiveStreamVideos() ),
    pushRoute: ( route, key ) => dispatch( pushRoute( route, key ) ),
    popRoute: key => dispatch( popRoute( key ) ),
  }
}

const mapStateToProps = state => ( {
  livestream_url: state.livestream.current_live_video_url,
  navigation: state.cardNavigation,
  videos: state.livestream.videos,
  loggedIn: state.auth.loggedIn,
} )

export default connect( mapStateToProps, bindAction )( LiveStream )
