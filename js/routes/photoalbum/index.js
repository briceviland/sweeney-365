import _ from "lodash/core"
import React, { Component } from "react"
import { connect } from "react-redux"
import {
  Image,
  Platform,
  TouchableOpacity,
  ListView,
} from "react-native"
import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  InputGroup,
  Input,
  View,
  Spinner,
} from "native-base"
import { actions } from "react-native-navigation-redux-helpers"

import { getAlbumImagesSmug } from "../../actions/smug"

import HeaderContent from "../../components/headerContent/"
import PhotoBrowser from "dsm-react-photo-browser"

import theme from "../../themes/base-theme"
import styles from "./styles"
import {TimerMixin} from 'react-timer-mixin';


const {
  popRoute,
} = actions

class PhotoAlbum extends Component {

  static propTypes = {
    album_key: React.PropTypes.string,
    getAlbumImagesSmug: React.PropTypes.func,
    popRoute: React.PropTypes.func,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
    } ),
  }

  constructor() {
    super()
    this.state = {
      images: new Array(),
      count: 175,
      page: 1,
      imagesLoaded: false
    }
    /*setTimeout(
        () => { alert('pop'); 
          this.state.images.push({
            caption: 'test',
            thumb: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
            // photo: image.ThumbnailUrl.replace('/Th/', '/L/').replace('-Th.jpg', '-L.jpg')
            photo: 'http://farm3.static.flickr.com/2667/4072710001_f36316ddc7_b.jpg',
          });
          this.setState(this.state.images);
        },
        1000
      );*/
  }

  componentDidMount() {
    this.props.getAlbumImagesSmug( this.props.album_key, this.state.count, this.state.page, true ).then(( images ) => {
      this.setState( { 
        images: images.images, 
        imagesLoaded: true, 
        total: images.total,
        page: this.state.page + 1
      } )
    })
  }

  popRoute() {
    this.props.popRoute( this.props.navigation.key )
  }

  loadMore(){
    if(this.state.imagesLoaded == true){
      this.state.imagesLoaded = false;
      this.props.getAlbumImagesSmug( this.props.album_key, this.state.count, this.state.page, true ).then(( moreImages ) => {
        this.setState({
          images: this.state.images.concat(moreImages.images), 
          page: this.state.page + 1, 
          imagesLoaded: true, 
          total: moreImages.total
        });
      })
    }
  }

  isCloseToBottom({layoutMeasurement, contentOffset, contentSize}){
    const paddingToBottom = 20;
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingToBottom;
  }

  render() {
    // : TODO Add catch to abort view if Photos don't load
    const images = this.state.images
    return (
      <Container theme={ theme } style={ styles.backgroundContainer } scrollEnabled={ false }>
        <Content style={ { marginBottom: ( Platform.OS === "ios" ) ? -50 : -10 } } scrollEnabled={ false }>
          <View scrollEnabled={ false }>
            <PhotoBrowser
              onBack={ () => this.popRoute() }
              mediaList={ images }
              displayNavArrows
              displaySelectionButtons={ false }
              displayActionButton={ false }
              startOnGrid
              enableGrid
              onLoadMoreAsync={ () => this.loadMore() }
              photoCount={this.state.total}
            />
            { images.length == 0 &&
              <View style={ styles.loadingView }>
                <Spinner color='white'/>
              </View>
            }
          </View>
        </Content>
      </Container>
    )
  } // : render()

}

function bindAction( dispatch ) {
  return {
    getAlbumImagesSmug: (album_key, count, page, total) => dispatch( getAlbumImagesSmug( album_key, count, page, total ) ),
    popRoute: key => dispatch( popRoute( key ) ),
  }
}

const mapStateToProps = state => ( {
  navigation: state.cardNavigation,
  count: state.count,
  page: state.page
} )

export default connect( mapStateToProps, bindAction )( PhotoAlbum )
