import _ from "lodash"
import React, { Component } from "react"
import {
  Image,
  Platform,
  TouchableOpacity,
  PushNotificationIOS,
  ViewPropTypes
} from "react-native"
import { connect } from "react-redux"
import { actions } from "react-native-navigation-redux-helpers"
import { Container, Content, List, ListItem, Text, InputGroup, Input, Button, View, Icon } from "native-base"
import { Grid, Col } from "react-native-easy-grid"
import Toast from "react-native-root-toast"
//import Orientation from 'react-native-orientation';

import SetupScreen from "../../routes/setupscreen"

import { actLoginAuth } from "../../actions/auth"

import theme from "../../themes/base-theme"
import styles from "./styles"

const {
  replaceAt,
  pushRoute,
} = actions

const bg = require( "../../../images/background.png" )
const logo = require( "../../../images/logo.png" )

class Login extends Component {

  static propTypes = {
    hasShownSetupScreen: React.PropTypes.bool.isRequired,
    actLoginAuth: React.PropTypes.func,
    replaceAt: React.PropTypes.func,
    pushRoute: React.PropTypes.func,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
    } ),
  }

  constructor( props ) {
    super( props )
    this.state = {
      username: "",
      password: "",
      loading:  false,
    }
    this.constructor.childContextTypes = {
      theme: React.PropTypes.object,
    }
    this.orientation = "PORTRAIT";

  }

  componentWillMount() {

    if ( Platform.OS === "android" ) {
      // TODO Implement android push notification check
    } else {
      PushNotificationIOS.checkPermissions( perms => {
        const hasPerms = _.includes( perms, 1 )
        this.setState( {
          pushNotificationsEnabled: hasPerms,
        } )
      } )
    }
  }
  componentDidMount(){
    
  }

  actLoginAuth() {
    let error = false
    if (this.state.username.trim().length == 0) {
      error = "Please enter your username."
    }
    else if (this.state.password.trim().length == 0) {
      error = "Please enter your password."
    }

    if ( error === false ) {
      this.setState( { loading: true } )
      let loadingToast = Toast.show( "Logging in...", {
        position: theme.statusBarHeight + 20,
        duration: 10000,
      })
      this.props.actLoginAuth( this.state.username, this.state.password ).then( ( loggedIn ) => {
        Toast.hide(loadingToast)
        if ( loggedIn === true ) {
          this.setState({
            username: "",
            password: "",
            loading: false,
          })
          Toast.hide( loadingToast )
          // Toast.show( "Logged in.", {
          //   position: theme.statusBarHeight + 20,
          // })
          if ( Platform.OS === "android" ) {
            this.pushRoute( "home" )
          }
          else if ( this.props.hasShownSetupScreen === true || this.state.pushNotificationsEnabled === true ) {
            this.pushRoute( "home" )
          }
          else {
            this.pushRoute( "setupscreen" )
          }
        }
        else {
          this.setState( { loading: false } )
          this._usernameInput._root.clear()
          this._passwordInput._root.clear()
          this._usernameInput._root.focus()
          Toast.show( "Invalid login, please try again.", {
            position: theme.statusBarHeight + 20,
          })
        }
      } )
    }
    else {
      Toast.show( error, {
        position: theme.statusBarHeight + 20,
      })
    }
  }

  replaceRoute( route ) {
    // : TODO Cull `replaceRoute` method in components/login/index
    this.props.replaceAt( "login", { key: route }, this.props.navigation.key )
  }

  pushRoute( route ) {
    this.props.pushRoute( { key: route, index: 1 }, this.props.navigation.key )
  }

  _renderMediaButtons = () => (
    <View style={ styles.mediaButtonsContainer }>
      <Grid>
        <Col>
          <TouchableOpacity full transparent style={ styles.mediaButton } onPress={ () => this.pushRoute( "livestream" ) }>
            <Icon name="video-camera" style={ styles.mediaButtonIcon } />
            <Text style={ styles.mediaButtonText }>Camp Video</Text>
          </TouchableOpacity>
        </Col>
        <Col>
          <TouchableOpacity full transparent style={ [ styles.mediaButton, styles.mediaButtonLast ] } onPress={ () => this.pushRoute( "smugmug" ) }>
            <Icon name="camera" style={ styles.mediaButtonIcon } />
            <Text style={ styles.mediaButtonText }>Camp Photos</Text>
          </TouchableOpacity>
        </Col>
      </Grid>
    </View>
  )

  render() {
    /*if(Orientation.getInitialOrientation() === 'LANDSCAPE'){
      styles.background = styles.backgroundlandscape;
      styles.logoAndroid = styles.logoAndroidlandscape;
      styles.logoIOS = styles.logoIOSlandscape;
      styles.container = styles.containerlandscape;
    }*/
    return (
        <Container>
          <Content theme={ theme } scrollEnabled={ false }>
            <Image source={ bg } style={ styles.background } >
              <Image source={ logo } style={ Platform.OS === "android" ? styles.logoAndroid : styles.logoIOS } />
              <View style={ styles.container }>
                { this._renderMediaButtons() }
                <List style={ styles.list }>
                  <ListItem style={ styles.listItem }>
                    <InputGroup borderType="rounded" style={ styles.inputGrp }>
                      <Icon
                        name="user"
                        style={ [ styles.inputIcon, this.state.loading ? styles.inputIconDisabled : null, { paddingLeft: 2 } ] }
                      />
                      <Input
                        ref={ (c) => this._usernameInput = c }
                        value={ this.state.username }
                        placeholder="Username"
                        autoCapitalize="none"
                        autoCorrect={ false }
                        returnKeyType={ "next" }
                        style={ [ styles.input, this.state.loading ? styles.inputDisabled : null ] }
                        onChangeText={ username => this.setState( { username: username } ) }
                        onSubmitEditing={ () => this._passwordInput._root.focus() }
                      />
                    </InputGroup>
                  </ListItem>
                  <ListItem style={ styles.listItem }>
                    <InputGroup borderType="rounded" style={ styles.inputGrp }>
                      <Icon
                        name="lock"
                        style={ [ styles.inputIcon, this.state.loading ? styles.inputIconDisabled : null, { paddingLeft: 2 } ] }
                      />
                      <Input
                        ref={ (c) => this._passwordInput = c }
                        value={ this.state.password }
                        placeholder="Password"
                        secureTextEntry
                        returnKeyType={ 'go' }
                        style={ [ styles.input, this.state.loading ? styles.inputDisabled : null ] }
                        onChangeText={ password => this.setState( { password: password } ) }
                        onSubmitEditing={ () => this.actLoginAuth() }
                      />
                    </InputGroup>
                  </ListItem>
                  <ListItem style={ styles.listItem }>
                    <Button
                      disabled={ this.state.loading }
                      rounded primary block large
                      style={ [ styles.loginButton, this.state.loading ? styles.loginButtonDisabled : null ] }
                      onPress={ () => this.actLoginAuth() }
                    >
                    <Text
                      style={ [
                        this.state.loading ? styles.loginButtonTextDisabled : null,
                        Platform.OS === "android" ? { marginTop: -4, fontSize: 16 } : { fontSize: 16, marginTop: -2, fontWeight: "900" }
                      ] }
                    >
                      GET STARTED
                    </Text>
                    </Button>
                  </ListItem>
                </List>
              </View>
            </Image>
          </Content>
        </Container>
    )
    
  }
}

function bindActions( dispatch ) {
  return {
    actLoginAuth: ( username, password ) => dispatch( actLoginAuth( username, password ) ),
    replaceAt: ( routeKey, route, key ) => dispatch( replaceAt( routeKey, route, key ) ),
    pushRoute: ( route, key ) => dispatch( pushRoute( route, key ) ),
  }
}

const mapStateToProps = state => ( {
  hasShownSetupScreen: state.auth.hasShownSetupScreen,
  navigation: state.cardNavigation,
} )

export default connect( mapStateToProps, bindActions )( Login )
