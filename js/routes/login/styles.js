import React from "react"
import { StyleSheet, Dimensions, Platform } from "react-native"

import theme from "../../themes/base-theme"

const SCREEN      = Dimensions.get( "window" )
const LOGO_WIDTH  = SCREEN.width * 0.65
const LOGO_RATIO  = 0.7
const LOGO_HEIGHT = LOGO_WIDTH * LOGO_RATIO
const FORM_HEIGHT = 300

module.exports = StyleSheet.create( {
  logoIOS: {
    width: LOGO_WIDTH,
    height: LOGO_HEIGHT,
    resizeMode: "contain",
    alignSelf: "center",
    marginTop: ( SCREEN.height - LOGO_HEIGHT - FORM_HEIGHT + theme.statusBarHeight ) / 2,
  },
  logoIOSlandscape: {
    width: LOGO_HEIGHT,
    height: LOGO_WIDTH,
    resizeMode: "contain",
    alignSelf: "center",
    marginTop: ( SCREEN.width - LOGO_WIDTH - FORM_HEIGHT + theme.statusBarHeight ) / 2,
  },
  logoAndroid: {
    width: LOGO_WIDTH,
    height: LOGO_HEIGHT,
    resizeMode: "contain",
    alignSelf: "center",
    marginTop: ( SCREEN.height - LOGO_HEIGHT - FORM_HEIGHT + theme.statusBarHeight ) / 2,
  },
  logoAndroidlandscape: {
    width: LOGO_HEIGHT,
    height: LOGO_WIDTH,
    resizeMode: "contain",
    alignSelf: "center",
    marginTop: ( SCREEN.width - LOGO_WIDTH - FORM_HEIGHT + theme.statusBarHeight ) / 2,
  },
  background: {
    flex: 1,
    width: null,
    height: SCREEN.height,
    backgroundColor: "rgba(0,0,0,0.1)",
  },
  backgroundlandscape: {
    flex: 1,
    width: null,
    height: SCREEN.width,
    backgroundColor: "rgba(0,0,0,0.1)",
  },
  container: {
    flex: 3,
    width: SCREEN.width,
    paddingVertical: 15,
    position: "absolute",
    bottom: 0,
    height: FORM_HEIGHT,
  },
  containerlandscape: {
    flex: 3,
    width: SCREEN.height,
    paddingVertical: 15,
    position: "absolute",
    bottom: 0,
    height: FORM_HEIGHT,
  },
  mediaButtonsContainer: {
    marginBottom: 15,
    paddingHorizontal: 15,
  },
  mediaButton: {
    flex: 1,
    alignSelf: "center",
    flexDirection: "column",
    alignItems: "center",
  },
  mediaButtonLast: {

  },
  mediaButtonIcon: {
    marginBottom: 5,
    fontSize: 24,
    opacity: 0.9,
  },
  mediaButtonText: {
    opacity: 0.9,
    fontSize: 14,
    fontWeight: "bold",
  },
  inputGroupContainer: {
    flex: 1,
    flexDirection: "column",
    marginBottom: 60,
  },
  inputGrp: {
    borderWidth: 0,
    backgroundColor: "rgba(255,255,255,0.47)",
    marginTop: 10,
    height: 48,
  },
  inputIcon: {
    opacity: 0.75,
    fontSize: 23,
    marginLeft: 15,
  },
  input: {
    paddingLeft: 15,
    fontSize: 16,
    fontWeight: '500',
    opacity: 0.9,
  },
  loginButton: {
    height: 48,
    marginRight: Platform.OS === "android" ? -10 : -13,
  },
  loginButtonDisabled: {
    backgroundColor: "#00a585",
  },
  loginButtonTextDisabled: {
    // color: "black",
  },
  listItem: {
    paddingLeft: 0,
    marginRight: 15,
  },
} )
