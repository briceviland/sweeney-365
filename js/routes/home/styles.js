const React = require( "react-native" )

const { StyleSheet, Dimensions, Platform } = React

const SCREEN = Dimensions.get( "window" )
const COLORS = require( "../../themes/variable" )

module.exports = StyleSheet.create( {
  backgroundContainer: {
    backgroundColor: COLORS.darkBackground,
  },
  camperCardsContainer: {
    padding: 7.5,
  },
  camperCard: {
    flex: 1,
    margin: 7.5,
    minHeight: 220,
  },
  camperCardSingle: {
    margin: 7.5,
  },
  camperCardFace: {
    padding: 15,
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",

    shadowRadius: 4,
    shadowColor: "#000",
    shadowOpacity: 0.25,
    shadowOffset: {
      width: 0,
      height: 0,
    },
  },
  camperCardFaceSingle: {
    padding: 20,
  },
  camperCardFront: {

  },
  profilePic: {
    width: 90,
    height: 90,
    borderRadius: 45,
    marginBottom: 10,
    borderWidth: 3,
    borderColor: "#fff",
  },
  profileUser: {
    alignSelf: "center",
    fontSize: 22,
    fontWeight: "bold",
  },
  profileUserInfo: {
    alignSelf: "center",
    opacity: 0.75,
    fontWeight: "bold",
  },
  profileInfoName: {
    fontSize: 17,
    fontWeight: "900",
    textAlign: "center",
  },
  profileUserCabin: {
    fontSize: 15,
    textAlign: "center",
  },
  camperCardBack: {
    backgroundColor: "#333",
    paddingVertical: 10,
    paddingHorizontal: 0,
  },
  scheduleIcon: {
    fontSize: 15,
    opacity: 0.75,
    color: "#fff",
    position: "absolute",
    top: 10,
    left: 10,
  },
  scheduleText: {
    textAlign: "center",
    lineHeight: 14,
    marginVertical: 3,
  },
  scheduleTextActive: {
    fontSize: 18,
    lineHeight: 18,
    // marginTop: 4,
    fontWeight: "700",
  },

  newsContainer: {
    margin: 7.5,
  },
  newsHeading: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
  },
  newsHeadingText: {
    fontSize: 21,
    fontWeight: "900",
  },
} )
