import moment from "moment"

import React, { Component } from "react"
import {
  Image,
  View,
  TouchableOpacity,
  Platform,
  RefreshControl,
  ScrollView,
  StatusBar,
} from "react-native"

import { Container, Header, Content, Text, Button, Card, Thumbnail, Spinner } from "native-base"
import Icon from "react-native-vector-icons/FontAwesome"

import FlipCard from "react-native-flip-card"

import { Grid, Col, Row } from "react-native-easy-grid"

import HeaderContent from "../../components/headerContent/"
import Post from "../../components/post"

import { connect } from "react-redux"
import { actions } from "react-native-navigation-redux-helpers"

import { openDrawer } from "../../actions/drawer"
import { toggleAllLoginActionsAuth } from "../../actions/auth"
import { actGetNewsFeedPosts } from "../../actions/news"
import { actGetCabinsInfo } from "../../actions/cabins"
import { actPostsNeedRefresh } from "../../actions/push"

import theme from "../../themes/base-theme"
import styles from "./styles"

var Orientation = require('react-native-orientation')

const {
  reset,
  pushRoute,
} = actions

const liveStreamImage = require( "../../../images/home-livestream.jpg" )

const CARD_COLORS = [ "#01cca1", "#2594f6", "#c83833", "#e86713" ]
const CARD_COLORS_INVERSE = [ ...CARD_COLORS ].reverse()

const getIndexForTimeNow = () => {
  const hour = new Date().getHours()
  return hour - 9
}

const insertCamperScheduleItems = ( schedule ) => {
  // return schedule
  const fullSchedule = [ ...schedule ]
  fullSchedule.splice( 0, 0, "breakfast" )
  fullSchedule.splice( 3, 0, "lunch" )
  fullSchedule.splice( fullSchedule.length, 0, "dinner" )
  return fullSchedule
}

class Home extends Component {

  static propTypes = {
    postsNeedRefresh: React.PropTypes.bool,
    campers: React.PropTypes.array,
    news: React.PropTypes.array,
    cabins: React.PropTypes.array,
    openDrawer: React.PropTypes.func,
    reset: React.PropTypes.func,
    resetAuth: React.PropTypes.func,
    pushRoute: React.PropTypes.func,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
    } ),
  }

  getCabinGalleries = cabins => {
    const galleries = []
    _.each( cabins, c => {
      galleries.push( ...c.galleries )
    } )
    return galleries
  }

  constructor( props ) {
    super( props )

    const news = this.props.news
    const galleries = this.getCabinGalleries( this.props.cabins )

    const posts = _.concat( news, galleries )
    // TODO Add loading for home page feed when checking for new posts
    this.state = {
      isRefreshing: true,
      posts: posts,
      isRefreshingPosts: false,
      isRefreshingGalleries: false,
    }
  }

  componentWillMount() {
    Orientation.unlockAllOrientations()
    const interval = setInterval(() => {
      StatusBar.setHidden( false, false )
    }, 500)

    this.setState( { 'statusBarInterval': interval } )
  }

  componentWillUnmount() {
    Orientation.lockToPortrait()
    clearInterval( this.state.statusBarInterval )
  }

  componentDidMount() {
    // TODO Add "last refreshed" time variable to not refresh too often
    this.refreshPosts()
  }

  componentWillReceiveProps( nextProps ) {
    if ( nextProps.postsNeedRefresh === true ) {
      this.refreshPosts()
      this.setState( {
        isRefreshing: true,
        isRefreshingPosts: true,
        isRefreshingGalleries: true,
      } )
      this.props.actPostsNeedRefresh( false )
      this._content.scrollTo( { x: 0, y: -60, animated: true } )
    }
  }

  pushRoute( route, passProps ) {
    this.props.pushRoute( { key: route, index: this.props.navigation.index + 1, passProps }, this.props.navigation.key )
  }

  logout() {
    this.props.reset( this.props.navigation.key )
    this.props.resetAuth()
  }

  refreshPosts() {
    const jointPosts = []

    this.props.actGetNewsFeedPosts().then( news => {
      _.each( news, post => jointPosts.push( post ) )
      if ( this.state.isRefreshingGalleries === false ) {
        this.setState( {
          posts: jointPosts,
          isRefreshing: false,
          isRefreshingPosts: false,
          isRefreshingGalleries: false,
        } )
        this.props.actPostsNeedRefresh( false )
      }
    } )
    this.props.actGetCabinsInfo().then( cabins => {
      const galleries = this.getCabinGalleries( cabins )
      _.each( galleries.slice( 0, 21 ), post => jointPosts.push( post ) )
      if ( this.state.isRefreshingPosts === false ) {
        this.setState( {
          posts: jointPosts,
          isRefreshing: false,
          isRefreshingPosts: false,
          isRefreshingGalleries: false,
        } )
        this.props.actPostsNeedRefresh( false )
      }
    } )
  }

  pullDownRefresh = () => {
    this.setState( { isRefreshing: true } )
    this.refreshPosts()
  }

  _renderCamperCol = ( camper, index, single ) => (
    <Col key={ camper.id } style={ [ styles.camperCard, single ? styles.camperCardSingle : null ] }>
      <FlipCard
        style={ { borderWidth: 0, borderColor: "transparent" } }
        flipHorizontal
        flipVertical={ false }
        alignHeight
      >
        { this._renderCamperCardFront( camper, index, single ) }
        { this._renderCamperCardBack( camper, index, single ) }
      </FlipCard>
    </Col>
    )

  _renderCamperRows = ( campers ) => {
    const content = new Array()
    for ( let i = 0; i < campers.length; i += 2 ) {
      content.push(
        <Row key={ i }>
          { this._renderCamperCol( campers[ i ], i, campers.length == 1 ) }
          { i < campers.length - 1 && this._renderCamperCol( campers[ i + 1 ], i + 1 ) }
        </Row>,
      )
    }
    return content
  }

  _renderCamperCardFront = ( camper, index, single ) => (
    <View style={ [ styles.camperCardFace, styles.camperCardFront, single ? styles.camperCardFaceSingle : null, { backgroundColor: CARD_COLORS[ index % CARD_COLORS.length ] } ] }>
      <Icon name="calendar" style={ styles.scheduleIcon } />
      <Thumbnail style={ styles.profilePic } source={ { uri: camper.photo_url } } />
      <View style={ [ styles.profileInfo, { marginLeft: 0, marginRight: 0 } ] }>
        <Text style={ styles.profileInfoName }>{ camper.name }</Text>
        <Text style={ styles.profileUserCabin }>{ camper.cabin } Cabin</Text>
      </View>
    </View>
    )

  _renderCamperCardBack = ( camper, index, single ) => (
    <View style={ [ styles.camperCardFace, styles.camperCardBack, single ? styles.camperCardFaceSingle : null ] }>
      <Icon name="calendar" style={ styles.scheduleIcon } />
      {
        _.map( insertCamperScheduleItems( camper.schedule ), ( activity, index ) => (
          <Text key={ index } style={ [ styles.scheduleText, index == getIndexForTimeNow() ? styles.scheduleTextActive : null ] }>{activity.toLowerCase()}</Text>
        ) )
      }
    </View>
    )

  _handlePostOnPress = post => {
    console.log(post.video_url)
    if ( _.isArray( post.images ) ) {
      this.pushRoute( "gallery", { images: post.images } )
    }
    else if ( ! post.video_url ) {
      this.pushRoute( "post", { post_id: post.id } )
    }
  }

  _renderPosts = posts => (
    <View style={ styles.newsContainer }>
      <View style={ styles.newsHeading }>
        <Text style={ styles.newsHeadingText }>CAMP FEED</Text>
      </View>
      {
        _.map( posts, ( post, index ) => {
          return (
            <Post.Post
              key={ index }
              onPress={ () => this._handlePostOnPress( post ) }
              backgroundColor={ CARD_COLORS_INVERSE[ index % CARD_COLORS_INVERSE.length ] }
              activeOpacity={ post.video_url ? 1 : 0.2 }
            >
              { post.video_url ?
                <Post.Video uri={ post.video_url } />
                :
                <Post.Image uri={ post.thumbnail ? post.thumbnail : post.image } />
              }
              <Post.Tags campers={ this.props.campers } tags={ post.tags } />
              <Post.Time time={ post.post_modified } />
              { post.video_url ?
                null
                :
                <Post.Title title={ post.post_title } />
              }
              { post.video_url ?
                null
                :
                <Post.Excerpt excerpt={ post.post_excerpt } />
              }
            </Post.Post>
          )
        } )
      }
    </View>
    )

  _renderLoading = () => (
    <Spinner color="white" />
  )

  render() {
    const campers = _.filter( this.props.campers, ( camper ) => camper.schedule.length > 0 )
    const posts = _.sortBy( this.state.posts, post => {
      return -moment( post.post_modified )
    } ).slice( 0, 41 )

    return (
      <Container theme={ theme } style={ styles.backgroundContainer }>
        <Header>
          <HeaderContent hideBack showLogout />
        </Header>
        <ScrollView
          ref={ ( c ) => { this._content = c } }
          style={ { marginBottom: ( Platform.OS === "ios" ) ? -50 : -10 } }
          refreshControl={
            <RefreshControl
              refreshing={ this.state.isRefreshing }
              onRefresh={ this.pullDownRefresh }
              tintColor={ '#fff' }
            />
          }
        >
          <View>
            <Grid style={ styles.camperCardsContainer }>
              { this._renderCamperRows( campers ) }
            </Grid>
          </View>
          { posts.length > 0 ?
            this._renderPosts( posts )
            :
            null
          }
        </ScrollView>
      </Container>
    )
  }

}

function bindAction( dispatch ) {
  return {
    actPostsNeedRefresh: ( needRefresh ) => dispatch( actPostsNeedRefresh( needRefresh ) ),
    actGetNewsFeedPosts: () => dispatch( actGetNewsFeedPosts() ),
    actGetCabinsInfo: () => dispatch( actGetCabinsInfo() ),
    openDrawer: () => dispatch( openDrawer() ),
    reset: key => dispatch( reset( [ { key: "login" } ], key, 0 ) ),
    resetAuth: () => dispatch( toggleAllLoginActionsAuth() ),
    pushRoute: ( route, key ) => dispatch( pushRoute( route, key ) ),
  }
}

const mapStateToProps = state => ( {
  postsNeedRefresh: state.push.postsNeedRefresh,
  navigation: state.cardNavigation,
  campers: _.filter( state.auth.campers, function(c) {
     return _.isEmpty( c ) === false
  }),
  news: state.news.posts,
  cabins: state.cabins.cabins,
} )

export default connect( mapStateToProps, bindAction )( Home )
