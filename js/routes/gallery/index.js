import _ from "lodash/core"
import React, { Component } from "react"
import {
  Image,
  Platform,
  TouchableOpacity,
  ListView,
} from "react-native"
import { connect } from "react-redux"
import { actions } from "react-native-navigation-redux-helpers"

import { getAlbumImagesSmug } from "../../actions/smug"

import { Container, Header, Content, Text, Button, Icon, InputGroup, Input, View } from "native-base"
import HeaderContent from "../../components/headerContent/"

import PhotoBrowser from "dsm-react-photo-browser"

import theme from "../../themes/base-theme"
import styles from "./styles"

const {
  popRoute,
} = actions

class Gallery extends Component {

  static propTypes = {
    images: React.PropTypes.array.isRequired,
    popRoute: React.PropTypes.func.isRequired,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
    } ).isRequired,
  }
  
  popRoute() {
    this.props.popRoute( this.props.navigation.key )
  }

  render() {
    const images = this.props.images

    return (
      <Container theme={ theme } style={ styles.backgroundContainer } scrollEnabled={ false }>
        <Content style={ { marginBottom: ( Platform.OS === "ios" ) ? -50 : -10 } } scrollEnabled={ false }>
          <View scrollEnabled={ false }>
            <PhotoBrowser
              onBack={ () => this.popRoute() }
              mediaList={ images }
              displayNavArrows
              displaySelectionButtons={ false }
              displayActionButton={ false }
              startOnGrid
              enableGrid
            />
            { images.length == 0 &&
              <View style={ styles.loadingView }>
                <Text>Loading...</Text>
              </View>
            }
          </View>
        </Content>
      </Container>
    )
  } // : render()

}

function bindAction( dispatch ) {
  return {
    popRoute: key => dispatch( popRoute( key ) ),
  }
}

const mapStateToProps = state => ( {
  navigation: state.cardNavigation,
} )

export default connect( mapStateToProps, bindAction )( Gallery )
