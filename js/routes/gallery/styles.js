const React = require( "react-native" )

const { StyleSheet, Dimensions, Platform } = React

const screen = Dimensions.get( "window" )
const darkBackground = require( "../../themes/variable" ).darkBackground

module.exports = StyleSheet.create( {
  backgroundContainer: {
    backgroundColor: darkBackground,
  },
  loadingView: {
    flex: 1,
    height: screen.height - 400,
    marginTop: 200,
    marginBottom: 200,
    width: screen.width,
    justifyContent: "center",
    alignItems: "center",
    zIndex: 0,
  },
} )
