import React, { Component } from "react"
import { Image } from "react-native"
const launchImage = require( "../../../images/launchImage.png" )

export default class SplashPage extends Component {

  static propTypes = {
    navigator: React.PropTypes.shape( {} ),
  }

  componentWillMount() {
    const navigator = this.props.navigator
  }

  render() { // eslint-disable-line class-methods-use-this
    return (
      <Image source={ launchImage } style={ { flex: 1, height: null, width: null } } />
    )
  }

}
