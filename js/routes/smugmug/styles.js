const React = require( "react-native" )

const { StyleSheet, Dimensions, Platform } = React

const primary = require( "../../themes/variable" ).brandPrimary
const darkBackground = require( "../../themes/variable" ).darkBackground
const screen = Dimensions.get( "window" )

module.exports = StyleSheet.create( {
  backgroundContainer: {
    backgroundColor: darkBackground,
  },
  headingContainer: {
    alignItems: "center",
    padding: 20,
  },
  headingText: {
    fontSize: 21,
    lineHeight: 21,
    marginTop: 2,
    fontWeight: "900",
  },
  subHeadingText: {
    fontSize: 19,
    lineHeight: 19,
  },
  channelImg: {
    height: ( screen.height / 4 ) + 10,
    width: ( screen.width / 2 ),
  },
  textShadow: {
    textShadowColor: '#222',
    textShadowOffset: { width: 1, height: 1 },
    textShadowRadius: 1,
  },
  ioschannelImgText: {
    lineHeight: 14,
    fontSize: 14,
    fontWeight: "900",
    padding: 10,
    paddingLeft: 0,
    paddingBottom: 0,
    marginBottom: 0,
    marginLeft: 10,
    marginTop: ( screen.height / 6 ) + 10,
  },
  achannelImgText: {
    lineHeight: 14,
    fontSize: 14,
    fontWeight: "900",
    marginLeft: 10,
    marginTop: ( screen.height / 5 ) - 10,
  },
} )
