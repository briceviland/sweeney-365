import _ from "lodash/core"
import React, { Component } from "react"
import { connect } from "react-redux"
import {
  Image,
  Platform,
  TouchableOpacity,
  RefreshControl,
  ListView,
} from "react-native"

import {
  Container,
  Header,
  Content,
  Text,
  View,
  Spinner,
} from "native-base"
import { Grid, Col, Row } from "react-native-easy-grid"

import { actions } from "react-native-navigation-redux-helpers"
import { getAlbumsListSmug } from "../../actions/smug"

import HeaderContent from "../../components/headerContent/"

import theme from "../../themes/base-theme"
import styles from "./styles" // : TODO Remove?

const {
  popRoute,
  pushRoute,
} = actions

class SmugMug extends Component {

  static propTypes = {
    albums: React.PropTypes.array,
    popRoute: React.PropTypes.func,
    pushRoute: React.PropTypes.func,
    getAlbums: React.PropTypes.func,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
    } ),
  }

  constructor( props ) {
    super( props )
    this.state = {
      albums: props.albums.length > 0 ? props.albums : new Array(),
      isRefreshing: false,
    }
  }

  componentWillMount() {
    this.pullDownRefresh()
  }

  pullDownRefresh = () => {
    this.setState( { isRefreshing: true } )
    this.refreshAlbums()
  }

  refreshAlbums = () => {
    this.props.getAlbums().then( ( albums ) => {
      this.setState( {
        albums,
        isRefreshing: false,
      } )
    } )
  }

  popRoute() {
    this.props.popRoute( this.props.navigation.key )
  }

  pushRoute( route, passProps ) {
    this.props.pushRoute( { key: route, index: this.props.navigation.index + 1, passProps }, this.props.navigation.key )
  }

  render() {
    const albums = _.sortBy( this.state.albums, "last_updated" ).reverse()
    // : TODO Use cached image
    const renderImageCol = ( album, i ) => (
      <Col key={ i }>
        <TouchableOpacity onPress={ () => this.pushRoute( "photoalbum", { album_key: album.key } ) }>
          <Image source={ { uri: album.thumbnail } } style={ styles.channelImg }>
            <Text style={ [ Platform.OS === "android" ? styles.achannelImgText : styles.ioschannelImgText, styles.textShadow ] }>{ album.title }</Text>
          </Image>
        </TouchableOpacity>
      </Col>
    )

    const content = []
    for ( let i = 0; i < albums.length; i += 2 ) {
      content.push(
        <Row key={ i }>
          { renderImageCol( albums[ i ], i ) }
          { i < albums.length - 1 && renderImageCol( albums[ i + 1 ], i + 1 ) }
        </Row>,
      )
    }

    return (
      <Container theme={ theme } style={ styles.backgroundContainer }>
        <Header>
          <HeaderContent showMenu={ this.props.loggedIn } />
        </Header>
        <Content
          style={ { marginBottom: ( Platform.OS === "ios" ) ? -50 : -10 } }
          refreshControl={
            <RefreshControl
              refreshing={ this.state.isRefreshing }
              onRefresh={ this.pullDownRefresh }
              tintColor={ "#fff" }
            />
          }
        >
          <Grid>
            <View style={ styles.headingContainer }>
              <Text style={ [ styles.headingText, styles.subHeadingText ] }>CAMP PHOTOS</Text>
            </View>
            { content }
          </Grid>
        </Content>
      </Container>
    )
  } // : render()

}

function bindAction( dispatch ) {
  return {
    getAlbums: () => dispatch( getAlbumsListSmug() ),
    pushRoute: ( route, key ) => dispatch( pushRoute( route, key ) ),
    popRoute: key => dispatch( popRoute( key ) ),
  }
}

const mapStateToProps = state => ( {
  navigation: state.cardNavigation,
  albums: state.smug.albums,
  loggedIn: state.auth.loggedIn,
} )

export default connect( mapStateToProps, bindAction )( SmugMug )
