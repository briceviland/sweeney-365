import _ from "lodash"
import React, { Component } from "react"
import {
  TouchableOpacity,
  Platform,
} from "react-native"
import { connect } from "react-redux"
import { actions } from "react-native-navigation-redux-helpers"

import { Container, Header, Content, Text, Button, Icon, InputGroup, Input, View, List, ListItem } from "native-base"
import Toast from 'react-native-root-toast'

import HeaderContent from "../../components/headerContent/"

import { actInviteUser } from "../../actions/auth"

import theme from "../../themes/base-theme"
import styles from "./styles"

const {
  popRoute,
} = actions

class InviteUser extends Component {

  static propTypes = {
    popRoute: React.PropTypes.func.isRequired,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
    } ).isRequired,
  }

  constructor( props ) {
    super( props )

    this.state = {
      loading: false,
      first_name: '',
      last_name: '',
      email: '',
    }
  }

  popRoute() {
    this.props.popRoute( this.props.navigation.key )
  }

  validateEmail =( email ) => {
    var re = /\S+@\S+\.\S+/
    return re.test( email )
  }

  inviteUser() {
    console.log('Invite User')
    const firstName = this.state.first_name
    const lastName  = this.state.last_name
    const email     = this.state.email

    let error = false
    if (firstName.trim().length == 0) {
      error = "Please enter a first name."
    }
    else if (lastName.trim().length == 0) {
      error = "Please enter a last name."
    }
    else if (email.trim().length == 0) {
      error = "Please enter an email."
    }
    else if ( this.validateEmail( email ) != true ) {
      error = "Please enter a valid email."
    }

    if ( error === false ) {
      this.setState( { loading: true } )
      const loadingToast = Toast.show( "Inviting user...", {
        position: theme.statusBarHeight + 20,
        duration: 10000,
      } )
      this.props.actInviteUser( firstName, lastName, email ).then( response => {
        this.setState( { loading: true } )
        Toast.hide( loadingToast )
        if ( response.error === true ) {
          Toast.show( "This email is registered, please enter a new one.", {
            position: theme.statusBarHeight + 20,
          })
          this._emailInput._root.clear()
          this._emailInput._root.focus()
        }
        else {
          Toast.show( "User invited.", {
            position: theme.statusBarHeight + 20,
          })
          this.setState({
            first_name: '',
            last_name: '',
            email: '',
          })
          this.popRoute()
        }
      } )
    }
    else {
      Toast.show( error, {
        position: theme.statusBarHeight + 20,
      })
    }

  }

  render() {

    return (
      <Container theme={ theme } style={ styles.backgroundContainer }>
        <Header>
          <HeaderContent />
        </Header>
        <Content style={ styles.contentContainer }>
          <View style={ styles.content }>
            <View style={ styles.headingContainer }>
              <Text style={ styles.headingText }>Invite User</Text>
            </View>
            <View>
              <List style={ styles.settingsList }>
                <ListItem style={ styles.listItem }>
                  <InputGroup borderType="rounded" style={ styles.inputGroup }>
                    <Input
                      ref={ (c) => this._firstNameInput = c }
                      placeholder="First Name"
                      autoCorrect={ false }
                      returnKeyType={ "next" }
                      style={ [ styles.input, this.state.loading ? styles.inputDisabled : null ] }
                      onChangeText={ value => this.setState( { first_name: value } ) }
                      onSubmitEditing={ () => this._lastNameInput._root.focus() }
                    />
                  </InputGroup>
                </ListItem>
                <ListItem style={ styles.listItem }>
                  <InputGroup borderType="rounded" style={ styles.inputGroup }>
                    <Input
                      ref={ (c) => this._lastNameInput = c }
                      placeholder="Last Name"
                      autoCorrect={ false }
                      returnKeyType={ "next" }
                      style={ [ styles.input, this.state.loading ? styles.inputDisabled : null ] }
                      onChangeText={ value => this.setState( { last_name: value } ) }
                      onSubmitEditing={ () => this._emailInput._root.focus() }
                    />
                  </InputGroup>
                </ListItem>
                <ListItem style={ styles.listItem }>
                  <InputGroup borderType="rounded" style={ styles.inputGroup }>
                    <Input
                      ref={ (c) => this._emailInput = c }
                      placeholder="Email"
                      autoCapitalize="none"
                      autoCorrect={ false }
                      returnKeyType={ "go" }
                      style={ [ styles.input, this.state.loading ? styles.inputDisabled : null ] }
                      onChangeText={ value => this.setState( { email: value } ) }
                      onSubmitEditing={ () => this.inviteUser() }
                    />
                  </InputGroup>
                </ListItem>
                <ListItem style={ styles.listItem }>
                  <Button
                    disabled={ this.state.loading }
                    rounded primary block large
                    style={ [ styles.submitButton, this.state.loading ? styles.submitButtonDisabled : null ] }
                    onPress={ () => this.inviteUser() }
                  >
                  <Text
                    style={ [
                      this.state.loading ? styles.submitButtonTextDisabled : null,
                      Platform.OS === "android" ? { marginTop: -8, fontSize: 16 } : { fontSize: 16, marginTop: -2, fontWeight: "900" }
                    ] }
                  >
                    GO
                  </Text>
                  </Button>
                </ListItem>
              </List>
            </View>
          </View>
        </Content>
      </Container>
    )
  } // : render()

}

function bindAction( dispatch ) {
  return {
    actInviteUser: ( first_name, last_name, email ) => dispatch( actInviteUser( first_name, last_name, email ) ),
    popRoute: key => dispatch( popRoute( key ) ),
  }
}

const mapStateToProps = state => ( {
  auth: state.auth,
  navigation: state.cardNavigation,
} )

export default connect( mapStateToProps, bindAction )( InviteUser )
