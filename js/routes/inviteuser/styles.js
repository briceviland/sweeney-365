const React = require( "react-native" )

const { StyleSheet, Dimensions, Platform } = React

const SCREEN = Dimensions.get( "window" )
const darkBackground = require( "../../themes/variable" ).darkBackground

module.exports = StyleSheet.create( {
  backgroundContainer: {
    backgroundColor: darkBackground,
  },
  contentContainer: {
    marginBottom: ( Platform.OS === "ios" ) ? -50 : -10,
  },
  content: {
    // padding: 15,
  },
  headingContainer: {
    alignItems: "center",
    padding: 20,
    paddingBottom: 10,
  },
  headingText: {
    fontSize: 21,
    lineHeight: 21,
    marginTop: 2,
    fontWeight: "900",
  },
  settingsList: {
    paddingRight: 15,
  },
  inputGroup: {
    borderWidth: 0,
    backgroundColor: "rgba(255,255,255,0.47)",
    marginTop: 10,
    height: 48,
  },
  inputIcon: {
    opacity: 0.75,
    fontSize: 23,
    marginLeft: 15,
  },
  input: {
    paddingLeft: 15,
    fontSize: 16,
    fontWeight: '500',
    opacity: 0.9,
  },
  submitButton: {
    height: 48,
    marginRight: -10,
  },
  submitButtonDisabled: {
    backgroundColor: "#00a585",
  },
  submitButtonTextDisabled: {
    // color: "black",
  },
} )
