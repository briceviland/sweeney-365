import _ from "lodash"
import React, { Component } from "react"
import {
  PushNotificationIOS,
  Switch,
  TouchableOpacity,
} from "react-native"
import { connect } from "react-redux"
import { actions } from "react-native-navigation-redux-helpers"

import { Container, Header, Content, Text, Button, Icon, InputGroup, Input, View, List, ListItem } from "native-base"
import HeaderContent from "../../components/headerContent/"

import { toggleAllLoginActionsAuth } from "../../actions/auth"

import theme from "../../themes/base-theme"
import styles from "./styles"

const {
  reset,
  popRoute,
  pushRoute,
} = actions

class Profile extends Component {

  static propTypes = {
    popRoute: React.PropTypes.func.isRequired,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
    } ).isRequired,
  }

  constructor( props ) {

    // https://github.com/zo0r/react-native-push-notification


    super( props )
    // console.log(PushNotificationIOS.checkPermissions())

    this.state = {
      pushNotificationsEnabled: true
    }

    // PushNotificationIOS.checkPermissions( perms => {
    //   console.log(perms)
    //   const hasPerms = _.includes(perms, 1)
    //   this.setState( {
    //     pushNotificationsEnabled: hasPerms,
    //     inAppNotificationsEnabled: false,
    //   } )
    // })
  }

  pushRoute( route, passProps ) {
    this.props.pushRoute( { key: route, index: this.props.navigation.index + 1, passProps }, this.props.navigation.key )
  }

  popRoute() {
    this.props.popRoute( this.props.navigation.key )
  }

  logout() {
    this.props.reset( this.props.navigation.key )
    this.props.resetAuth()
  }

  setPushNotificationState = value => {
    this.setState( { "pushNotificationsEnabled": value } )
    // if ( value === true ) {
    //   PushNotificationIOS.requestPermissions()
    //
    //   PushNotificationIOS.checkPermissions( perms => {
    //     console.log(perms)
    //     this.setState( { "pushNotificationsEnabled": true } )
    //     console.log('SET PUSH', value)
    //   } )
    // }
    // else if ( value === false ) {
    //   PushNotificationIOS.abandonPermissions()
    //
    //   PushNotificationIOS.checkPermissions( perms => {
    //     console.log(perms)
    //     this.setState( { "pushNotificationsEnabled": false } )
    //
    //     console.log('ABBANDON', value)
    //   } )
    // }
  }

  setInAppNotificationState = value => {
    this.setState( { "inAppNotificationsEnabled": value } )
  }

  render() {

    const pushNotificationsEnabled = this.state.pushNotificationsEnabled
    const inAppNotificationsEnabled = this.state.inAppNotificationsEnabled
    // console.log(pushNotificationsEnabled)

    return (
      <Container theme={ theme } style={ styles.backgroundContainer }>
        <Header>
          <HeaderContent />
        </Header>
        <Content style={ styles.contentContainer }>
          <View style={ styles.content }>
            <View style={ styles.headingContainer }>
              <Text style={ styles.headingText }>Profile</Text>
            </View>
            <View>
              <List style={ styles.settingsList }>
                <ListItem itemDivider style={ [ styles.settingsItem, styles.settingsDividerItem ]}>
                  <Text style={ styles.settingsDividerText }>Actions</Text>
                </ListItem>
                <ListItem
                  iconLeft
                  style={ styles.settingsItem }
                  onPress={ () => this.pushRoute( "inviteuser" ) }
                >
                  <Icon name="user-plus" style={ [ styles.settingsIcon, styles.settingsIconUserPlus ] } />
                  <Text style={ styles.settingsText }>Invite User</Text>
                </ListItem>
                <ListItem
                  iconLeft
                  style={ styles.settingsItem }
                  onPress={ () => this.logout() }
                >
                  <Icon name="sign-out" style={ [ styles.settingsIcon, styles.settingsIconSignOut ] } />
                  <Text style={ styles.settingsText }>Log Out</Text>
                </ListItem>
            </List>
            </View>
          </View>
        </Content>
      </Container>
    )
  } // : render()

}

// <ListItem itemDivider style={ [ styles.settingsItem, styles.settingsDividerItem ]}>
//   <Text style={ styles.settingsDividerText }>Settings</Text>
// </ListItem>
// <ListItem iconLeft style={ styles.settingsItem }>
//   <Icon name="bell" style={ styles.settingsIcon } />
//   <Text style={ styles.settingsText }>Push Notifications</Text>
//     <Switch
//       onValueChange={ value => this.setPushNotificationState(value) }
//       value={ pushNotificationsEnabled } />
// </ListItem>
// <ListItem iconLeft style={ styles.settingsItem }>
//   <Icon name="square" style={ styles.settingsIcon } />
//   <Text style={ styles.settingsText }>In-App Notifications</Text>
//     <Switch
//       onValueChange={ value => this.setInAppNotificationState(value) }
//       value={ pushNotificationsEnabled } />
// </ListItem>

function bindAction( dispatch ) {
  return {
    popRoute: key => dispatch( popRoute( key ) ),
    reset: key => dispatch( reset( [ { key: "login" } ], key, 0 ) ),
    resetAuth: () => dispatch( toggleAllLoginActionsAuth() ),
    pushRoute: ( route, key ) => dispatch( pushRoute( route, key ) ),
  }
}

const mapStateToProps = state => ( {
  auth: state.auth,
  navigation: state.cardNavigation,
} )

export default connect( mapStateToProps, bindAction )( Profile )
