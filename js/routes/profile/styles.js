const React = require( "react-native" )

const { StyleSheet, Dimensions, Platform } = React

const SCREEN = Dimensions.get( "window" )
const darkBackground = require( "../../themes/variable" ).darkBackground

module.exports = StyleSheet.create( {
  backgroundContainer: {
    backgroundColor: darkBackground,
  },
  contentContainer: {
    marginBottom: ( Platform.OS === "ios" ) ? -50 : -10,
  },
  content: {
    // padding: 15,
  },
  headingContainer: {
    alignItems: "center",
    padding: 20,
    paddingBottom: 0,
  },
  headingText: {
    fontSize: 21,
    lineHeight: 21,
    marginTop: 2,
    fontWeight: "900",
  },
  settingsList: {

  },
  settingsDividerItem: {
    marginTop: 10,
  },
  settingsIcon: {
    fontSize: 30,
    width: 40,
    textAlign: "center",
  },
  settingsText: {
    fontWeight: "500",
    fontSize: 18,
    lineHeight: Platform.OS === "android" ? 25 : 30,
  },
  settingsDividerText: {
    fontWeight: "700",
    fontSize: 18,
  },
  settingsIconSignOut: {
    transform: [ { rotate: "180deg" } ],
  },
} )
