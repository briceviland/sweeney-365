import _ from "lodash"
import React, { Component } from "react"
import {
  Image,
  Platform,
  TouchableOpacity,
} from "react-native"

import { connect } from "react-redux"
import { Container, Header, Content, Text, View, Thumbnail } from "native-base"
import { Grid, Col, Row } from "react-native-easy-grid"

import Icon from "react-native-vector-icons/FontAwesome"

import { actions } from "react-native-navigation-redux-helpers"

import HeaderContent from "../../components/headerContent/"
import Post from "../../components/post"

import theme from "../../themes/base-theme"
import styles from "./styles"

const {
  popRoute,
  pushRoute,
} = actions

const CARD_COLORS = [ "#01cca1", "#2594f6", "#c83833", "#e86713" ]

class Cabin extends Component {

  static propTypes = {
    cabin: React.PropTypes.object.isRequired,
    campers: React.PropTypes.array.isRequired,
    campersOf: React.PropTypes.array,
    popRoute: React.PropTypes.func.isRequired,
    pushRoute: React.PropTypes.func.isRequired,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
      index: React.PropTypes.number,
    } ).isRequired,
  }

  constructor( props ) {
    super( props )
    const rand = _.random( 0, CARD_COLORS.length -1 )
    const rand2 = rand + 1 == CARD_COLORS.length ? 0 : rand + 1
    this.state = {
      buttonBackgroundColor: CARD_COLORS[ rand ],
      randStart: rand2,
    }
  }

  popRoute() {
    this.props.popRoute( this.props.navigation.key )
  }

  pushRoute( route, passProps ) {
    this.props.pushRoute( {
      key: route,
      index: this.props.navigation.index + 1,
      passProps,
    }, this.props.navigation.key )
  }

  _renderGalleries = galleries => (
    <View style={ styles.galleriesContainer }>
      <View style={ styles.headingContainer }>
        <Text style={ [ styles.headingText, styles.subHeadingText ] }>GALLERIES</Text>
      </View>
      {
        _.map( galleries, ( gallery, index ) => (
          <Post.Post
            key={ index }
            onPress={ () => this.pushRoute( "gallery", { images: gallery.images } ) }
            backgroundColor={ CARD_COLORS[ ( index + this.state.randStart ) % CARD_COLORS.length ] }
          >
            <Post.Image uri={ gallery.thumbnail } />
            <Post.Tags campers={ this.props.campers } tags={ gallery.tags } />
            <Post.Time time={ gallery.post_modified } />
            <Post.Title title={ gallery.post_title } />
          </Post.Post>
        ) )
      }
    </View>
    )

  _renderCampersOf = () => {
    if ( this.props.campersOf.length > 0 ) {

      const day = _.find( this.props.campersOf, { type: 'day' } ) || false
      const week = _.find( this.props.campersOf, { type: 'week' } ) || false

      const content = []
      if ( day ) {
        content.push(
          <Col key={ 'day' } style={ styles.camperOfCol }>
            <Text style={ styles.camperOfType }>Day</Text>
            <Thumbnail style={ styles.camperOfThumb } source={ { uri: day.photo_url } } />
            <Text style={ styles.camperOfName }>{ day.name }</Text>
          </Col>
        )
      }
      if ( week ) {
        content.push(
          <Col key={ 'week' } style={ styles.camperOfCol }>
            <Text style={ styles.camperOfType }>Week</Text>
            <Thumbnail style={ styles.camperOfThumb } source={ { uri: week.photo_url } } />
            <Text style={ styles.camperOfName }>{ week.name }</Text>
          </Col>
        )
      }

      return (
        <View style={ styles.camperOfTheContainer }>
          { ( day || week ) &&
            <Text style={ styles.camperOfHeading }>Camper of the...</Text>
          }
          <Grid>
            { content }
          </Grid>
        </View>
      )
    }
  }

  render() {
    const cabin = this.props.cabin
    const cabinName = `${ cabin.name.replace( 'Cabin', '' ) } Cabin`

    return (
      <Container theme={ theme } style={ styles.backgroundContainer }>
        <Header>
          <HeaderContent />
        </Header>
        <Content style={ { marginBottom: ( Platform.OS === "ios" ) ? -50 : -10 } }>
          <View
            style={ [
              styles.headingContainer,
              { backgroundColor: cabin.color }
            ] }
          >
            <Text style={ styles.headingText }>{ cabinName }</Text>
          </View>
          {
            cabin.hero &&
            <Image source={{ uri: cabin.hero }} style={ styles.cabinHero }/>
          }
          <View style={ styles.viewStaffContainer }>
            <TouchableOpacity
              style={ [ styles.viewStaffButton, { backgroundColor: this.state.buttonBackgroundColor } ] }
              onPress={ () => this.pushRoute( "staff", { cabin_id: cabin.id } ) }
            >
              <Text style={ styles.videoHeaderTitle }>View Staff</Text>
              <Icon name="chevron-right" style={ styles.buttonIcon } />
            </TouchableOpacity>
            { this._renderCampersOf() }
          </View>
          <View>
            { cabin.galleries.length > 0 && this._renderGalleries( _.sortBy( cabin.galleries, 'post_modified' ).reverse() ) }
          </View>
        </Content>
      </Container>
    )
  } // : render()

}

function bindAction( dispatch ) {
  return {
    pushRoute: ( route, key ) => dispatch( pushRoute( route, key ) ),
    popRoute: key => dispatch( popRoute( key ) ),
  }
}

const mapStateToProps = ( state, ownProps ) => {
  const cabin = _.find( state.cabins.cabins, { id: ownProps.cabin_id } )
  return ( {
    navigation: state.cardNavigation,
    campersOf: _.filter( state.cabins.campersOf, { cabin: cabin.name } ),
    campers: state.auth.campers,
    cabin: cabin,
  } )
}

export default connect( mapStateToProps, bindAction )( Cabin )
