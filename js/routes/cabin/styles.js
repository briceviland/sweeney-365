const React = require( "react-native" )

const { StyleSheet, Dimensions, Platform } = React

const SCREEN = Dimensions.get( "window" )
const COLORS = require( "../../themes/variable" )

module.exports = StyleSheet.create( {
  backgroundContainer: {
    backgroundColor: COLORS.darkBackground,
  },
  shadowBase: {
    shadowRadius: 4,
    shadowColor: "#000",
    shadowOpacity: 0.25,
    shadowOffset: {
      width: 0,
      height: 0,
    },
  },
  cabinContainer: {
    margin: 7.5,
  },
  cabinHero: {
    width: SCREEN.width,
    height: SCREEN.width * ( 550 / 1920 ) * 1.5,
  },

  headingContainer: {
    // flex: 1,
    alignItems: "center",
    // justifyContent: "center",
    padding: 20,
  },
  headingText: {
    fontSize: 21,
    lineHeight: 21,
    marginTop: 2,
    fontWeight: "900",
  },
  subHeadingText: {
    fontSize: 19,
    lineHeight: 19,
  },
  textShadow: {
    textShadowColor: '#222',
    textShadowOffset: { width: 1, height: 1 },
    textShadowRadius: 1,
  },
  viewStaffContainer: {
    // marginHorizontal: 15,
    // marginTop: 15,
  },
  viewStaffButton: {
    padding: 15,
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  camperOfTheContainer: {
    // padding: 15,
  },
  videoHeaderTitle: {
    flex: 11,
    fontWeight: "900",
    fontSize: 17,
    alignSelf: "flex-start",
  },
  buttonIcon: {
    flex: 1,
    fontSize: 19,
    color: "#fff",
    alignSelf: "flex-end",
  },

  galleriesContainer: {
    margin: 7.5,
  },
  camperOfHeading: {
    paddingTop: 15,
    paddingVertical: 15,
    textAlign: 'center',
    fontSize: 20,
    fontWeight: '700',
  },
  camperOfCol: {
    alignItems: 'center',
    paddingHorizontal: 5,
    paddingBottom: 0,
  },
  camperOfType: {
    textAlign: 'center',
    fontWeight: '500',
    fontSize: 15,
  },
  camperOfThumb: {
    height: 100,
    width: 100,
    borderRadius: 50,
    borderColor: '#fff',
    borderWidth: 2,
    marginVertical: 10,
  },
  camperOfName: {
    fontWeight: '500',
    fontSize: 14,
    textAlign: 'center',
  },
} )
