const React = require( "react-native" )

const { StyleSheet, Dimensions, Platform } = React

const SCREEN = Dimensions.get( "window" )
const darkBackground = require( "../../themes/variable" ).darkBackground
const infoColor = require( "../../themes/variable" ).info

module.exports = StyleSheet.create( {
  backgroundContainer: {
    backgroundColor: "#07589e",
  },
  contentContainer: {
    marginBottom: ( Platform.OS === "ios" ) ? -50 : -10,
  },
  content: {
    // padding: 15,
  },
  background: {
    flex: 1,
    width: null,
    height: SCREEN.height,
  },
  swiperDot: {
    backgroundColor:'rgba(0,0,0,.8)',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 0
  },
  swiperActiveDot: {
    backgroundColor: '#fff',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 0
  },
  slide: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
  },
  slideHeadingSmallMargin: {
    marginBottom: 20,
  },
  slideHeading: {
    fontSize: 26,
    lineHeight: 32,
    fontWeight: "700",
    textAlign: "center",
    marginBottom: 20,
  },
  slideHeadingSmall: {
    fontSize: 22,
    lineHeight: 26,
    fontWeight: "700",
    textAlign: "center",
    marginBottom: 30,
  },
  slideHeadingSmallest: {
    fontSize: 14,
    lineHeight: 14,
    // fontWeight: "500",
    textAlign: "center",
    marginTop: 30,
    marginBottom: -30,
  },
  slideButton: {
    width: 200,
    alignSelf: "center",
  },
  slideButtonSmall: {
    width: 150,
    alignSelf: "center",
    marginBottom: 10,
  },
  slideButtonNegative: {
    backgroundColor: "#c83833",
  },
  slideButtonText: {
    fontSize: 18,
    fontWeight: "700",
  },
  pushNotificationsList: {
    paddingHorizontal: ( SCREEN.width - 250 ) / 2,
    marginBottom: 30,
    alignSelf: "flex-start",
  },
  pushNotificationsListItem: {
    borderBottomWidth: 0,
  },
  pushNotificationsListItemText: {
    fontWeight: "500",
  },
} )
