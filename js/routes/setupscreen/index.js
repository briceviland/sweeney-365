import _ from "lodash"
import React, { Component } from "react"
import {
  Image,
  Switch,
  TouchableOpacity,
  PushNotificationIOS,
} from "react-native"
import { connect } from "react-redux"
import { actions } from "react-native-navigation-redux-helpers"

import Swiper from 'react-native-swiper'
import { Container, Header, Content, Text, Button, Icon, View, List, ListItem, CheckBox } from "native-base"

// var PushNotification = require('react-native-push-notification')

import { toggleHasShownSetup } from "../../actions/auth"
import { setNotificationChannels } from "../../actions/push"

import theme from "../../themes/base-theme"
import styles from "./styles"

const {
  replaceAt,
  pushRoute,
} = actions

const bg = require( "../../../images/background-sideBar.png" )

class SetupScreen extends Component {

  static propTypes = {
    toggleHasShownSetup: React.PropTypes.func.isRequired,
    setNotificationChannels: React.PropTypes.func.isRequired,
    replaceAt: React.PropTypes.func.isRequired,
    pushRoute: React.PropTypes.func.isRequired,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
      index: React.PropTypes.integer,
    } ).isRequired,
  }

  constructor( props ) {
    super( props )

    this.state = {
      swiper: null,
      channels: {
        tags: true,
        livestream: true,
        messages: true,
      }
    }
  }

  replaceRoute( route, passProps ) {
    this.props.replaceAt(
      "setupscreen",
      {
        key: route,
        index: this.props.navigation.index,
        passProps,
      },
      this.props.navigation.key
    )
  }

  pushRoute( route, passProps ) {
    this.props.pushRoute( {
      key: route,
      index: this.props.navigation.index + 1,
      passProps,
    }, this.props.navigation.key )
  }

  requestPushNotificationPermissions() {
    PushNotificationIOS.requestPermissions()
    this._swiper.scrollBy( 1 )
  }

  togglePushNotificationChannel( key ) {
    let channels = this.state.channels
    channels[key] = channels[key] ? false : true
    this.setState({
      channels: channels,
    })
  }

  completeSetupScreen() {
    this.props.toggleHasShownSetup( true )
    this.props.setNotificationChannels( _(this.state.channels).pickBy().keys().value() )
    // this.pushRoute( "home" )
    this.replaceRoute( "home" )
  }

  render() {

    return (
      <Container theme={ theme } style={ styles.backgroundContainer }>
        <Image source={ bg } style={ styles.background }>
          <Content style={ styles.contentContainer } scrollEnabled={false}>
            <View style={ styles.content }>
              <Swiper
                ref={ c => { this._swiper = c } }
                style={ styles.swiper }
                loop={false}
                scrollEnabled={false}
                showsPagination={false}
              >
                <View style={ styles.slide }>
                  <Text style={ styles.slideHeading }>Welcome to the Camp Sweeney App!</Text>
                  <Button
                    style={ styles.slideButton }
                    rounded primary
                    onPress={ () => { this._swiper.scrollBy( 1 ) } }
                  >
                    <Text style={ styles.slideButtonText }>Next</Text>
                  </Button>
                </View>
                <View style={ styles.slide }>
                  <Text style={ styles.slideHeadingSmall }>Would you like to turn on Push Notifications?</Text>
                  <Button
                    style={ styles.slideButtonSmall }
                    rounded
                    onPress={ () => { this.requestPushNotificationPermissions() } }
                  >
                    <Text style={ styles.slideButtonText }>Yes</Text>
                  </Button>
                  <Button
                    style={ [ styles.slideButtonSmall, styles.slideButtonNegative ] }
                    rounded
                    onPress={ () => { this._swiper.scrollBy( 1 ) } }
                  >
                    <Text style={ styles.slideButtonText }>No</Text>
                  </Button>
                  <Text style={ styles.slideHeadingSmallest }>(You can always enable push notifications through your phone settings later.)</Text>
                </View>
                <View style={ styles.slide }>
                  <Text style={ styles.slideHeading }>All Done!</Text>
                  <Button
                    style={ styles.slideButton }
                    rounded primary
                    onPress={ () => { this.completeSetupScreen() } }
                  >
                    <Text style={ styles.slideButtonText }>Finish</Text>
                  </Button>
                </View>
              </Swiper>
            </View>
          </Content>
        </Image>
      </Container>
    )
  } // : render()

}

function bindAction( dispatch ) {
  return {
    toggleHasShownSetup: ( value ) => dispatch( toggleHasShownSetup( value ) ),
    setNotificationChannels: ( channels ) => dispatch( setNotificationChannels( channels ) ),
    replaceAt: ( currentKey, route, key ) => dispatch( replaceAt( currentKey, route, key ) ),
    pushRoute: ( route, passProps ) => dispatch( pushRoute( route, passProps ) ),
  }
}

const mapStateToProps = state => ( {
  navigation: state.cardNavigation,
} )

export default connect( mapStateToProps, bindAction )( SetupScreen )
