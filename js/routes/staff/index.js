import _ from "lodash/core"
import React, { Component } from "react"
import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
} from "react-native"

import { connect } from "react-redux"
import { Container, Header, Content, Text, View } from "native-base"
import { Grid, Col, Row } from "react-native-easy-grid"
import HTMLView from "react-native-htmlview"

import Icon from "react-native-vector-icons/FontAwesome"

import { actions } from "react-native-navigation-redux-helpers"
import { actGetCabinsInfo } from "../../actions/cabins"

import HeaderContent from "../../components/headerContent/"

import theme from "../../themes/base-theme"
import styles from "./styles"

const {
  popRoute,
  pushRoute,
} = actions

const CARD_COLORS = [ "#01cca1", "#2594f6", "#c83833", "#e86713" ]

const htmlViewStylesheet = StyleSheet.create({
  body: {
    color: "#fff",
  },
  p: {
    color: "#fff",
  }
})

class Staff extends Component {

  static propTypes = {
    cabin: React.PropTypes.object.isRequired,
    popRoute: React.PropTypes.func.isRequired,
    pushRoute: React.PropTypes.func.isRequired,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
      index: React.PropTypes.number,
    } ).isRequired,
  }

  popRoute() {
    this.props.popRoute( this.props.navigation.key )
  }

  pushRoute( route, passProps ) {
    this.props.pushRoute( {
      key: route,
      index: this.props.navigation.index + 1,
      passProps,
    }, this.props.navigation.key )
  }

  render() {
    const cabinName = this.props.cabin.name
    const cabinStaff = this.props.cabin.staff

    return (
      <Container theme={ theme } style={ styles.backgroundContainer }>
        <Header>
          <HeaderContent showMenu={ false } />
        </Header>
        <Content style={ { marginBottom: ( Platform.OS === "ios" ) ? -50 : -10 } }>
          <View style={ styles.cabinContainer }>
            <View style={ styles.cabinHeading }>
              <Text style={ styles.cabinHeadingText }>{ cabinName } Staff</Text>
            </View>
            {
              _.map(cabinStaff, ( staff, index ) => (
                <TouchableOpacity key={ index } onPress={ () => this.pushRoute( "staff", { staff_id: staff.id } ) }>
                  <View
                    style={ [
                      styles.staffContainer,
                      { backgroundColor: CARD_COLORS[ index % CARD_COLORS.length ] }
                    ] }
                  >
                    { staff.thumbnail &&
                      <Image style={ styles.staffImage } source={ { uri: staff.thumbnail } } />
                    }
                    <View style={ styles.staffInfo }>
                      <Text numberOfLines={ 1 } style={ styles.staffName }>
                        { staff.post_title }
                      </Text>
                      <HTMLView
                        stylesheet={ htmlViewStylesheet }
                        value={ staff.post_content }
                        onLinkPress={ url => this.pushRoute( "webviewer", { url } ) }
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              ) )
            }
          </View>
        </Content>
      </Container>
    )
  } // : render()

}

function bindAction( dispatch ) {
  return {
    pushRoute: ( route, key ) => dispatch( pushRoute( route, key ) ),
    popRoute: key => dispatch( popRoute( key ) ),
  }
}

const mapStateToProps = ( state, ownProps ) => ( {
  navigation: state.cardNavigation,
  cabin: _.find( state.cabins.cabins, { id: ownProps.cabin_id } ),
} )

export default connect( mapStateToProps, bindAction )( Staff )
