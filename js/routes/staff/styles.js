const React = require( "react-native" )

const { StyleSheet, Dimensions, Platform } = React

const SCREEN = Dimensions.get( "window" )
const COLORS = require( "../../themes/variable" )

module.exports = StyleSheet.create( {
  backgroundContainer: {
    backgroundColor: COLORS.darkBackground,
  },
  shadowBase: {
    shadowRadius: 4,
    shadowColor: "#000",
    shadowOpacity: 0.25,
    shadowOffset: {
      width: 0,
      height: 0,
    },
  },
  cabinContainer: {
    margin: 7.5,
  },
  cabinHeading: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
    marginTop: 5,
  },
  cabinHeadingText: {
    fontSize: 21,
    fontWeight: "900",
  },
  staffContainer: {
    flex: 1,
    flexDirection: "column",
    margin: 7.5,
  },
  staffImage: {
    height: ( SCREEN.width - ( 7.5 * 2 ) ) * ( 280 / 300 ),
  },
  staffInfo: {
    padding: 15,
  },
  staffName: {
    // padding: 15,
    marginBottom: 15,
    color: "#fff",
    fontWeight: "bold",
    fontSize: 18,
  },
  staffContent: {
    color: "#fff",
  },
} )
