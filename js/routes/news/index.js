import moment from "moment"

import React, { Component } from "react"
import {
  Image,
  View,
  TouchableOpacity,
  Platform,
  RefreshControl,
  StatusBar,
} from "react-native"

import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Card,
  Thumbnail,
  Spinner,
} from "native-base"
import Icon from "react-native-vector-icons/FontAwesome"

import HeaderContent from "../../components/headerContent/"
import Post from "../../components/post"

import { connect } from "react-redux"
import { actions } from "react-native-navigation-redux-helpers"

import { openDrawer } from "../../actions/drawer"
import { actGetNewsFeedPosts } from "../../actions/news"

import theme from "../../themes/base-theme"
import styles from "./styles"

var Orientation = require('react-native-orientation')

const {
  reset,
  pushRoute,
} = actions

const liveStreamImage = require( "../../../images/home-livestream.jpg" )

const CARD_COLORS = [ "#01cca1", "#2594f6", "#c83833", "#e86713" ]
const CARD_COLORS_INVERSE = [ ...CARD_COLORS ].reverse()


class Home extends Component {

  static propTypes = {
    campers: React.PropTypes.array.isRequired,
    posts: React.PropTypes.array.isRequired,
    openDrawer: React.PropTypes.func.isRequired,
    reset: React.PropTypes.func.isRequired,
    resetAuth: React.PropTypes.func.isRequired,
    pushRoute: React.PropTypes.func.isRequired,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
      number: React.PropTypes.number,
    } ).isRequired,
  }

  constructor( props ) {
    super( props )
    this.state = {
      posts: this.props.posts,
      isRefreshing: false,
    }
  }

  componentWillMount() {
    Orientation.unlockAllOrientations()
    const interval = setInterval(() => {
      StatusBar.setHidden( false, false )
    }, 500)

    this.setState( { 'statusBarInterval': interval } )
  }

  componentWillUnmount() {
    Orientation.lockToPortrait()
    clearInterval( this.state.statusBarInterval )
  }

  componentDidMount() {
    setTimeout( this.pullDownRefresh, 200 )
  }

  pushRoute( route, passProps ) {
    this.props.pushRoute( {
      key: route,
      index: this.props.navigation.index + 1,
      passProps,
    }, this.props.navigation.key )
  }

  refreshPosts = () => {
    this.props.dispatch( actGetNewsFeedPosts() ).then( ( posts ) => {
      this.setState( {
        posts: posts,
        isRefreshing: false,
      } )
    } )
  }

  pullDownRefresh = () => {
    this.setState( { isRefreshing: true } )
    this.refreshPosts()
  }

  _renderCamperTags = tags => {
    const images = []
    _.each( this.props.campers, ( camper, index ) => {
      if ( tags.indexOf( camper.id.toString() ) > -1 ) {
        images.push(
          <Thumbnail key={ index } style={ styles.camperTagImage } source={ { uri: camper.photo_url } } />
        )
      }
    } )
    if ( images.length > 0) {
      return (
        <View style={ styles.camperTags }>
          { images }
        </View>
      )
    }
  }

  _handlePostOnPress = post => {
    console.log(post.video_url)
    if ( _.isArray( post.images ) ) {
      this.pushRoute( "gallery", { images: post.images } )
    }
    else if ( ! post.video_url ) {
      this.pushRoute( "post", { post_id: post.id } )
    }
  }

  _renderNewsPosts = posts => (
    <View style={ styles.newsContainer }>
      <View style={ styles.newsHeading }>
        <Text style={ styles.newsHeadingText }>CAMP NEWS</Text>
      </View>
      {
        _.map( posts, ( post, index ) => (
          <Post.Post
            key={ index }
            onPress={ () => this._handlePostOnPress( post ) }
            backgroundColor={ CARD_COLORS_INVERSE[ index % CARD_COLORS_INVERSE.length ] }
            activeOpacity={ post.video_url ? 1 : 0.2 }
          >
          { post.video_url ?
            <Post.Video uri={ post.video_url } />
            :
            <Post.Image uri={ post.thumbnail ? post.thumbnail : post.image } />
          }
          <Post.Tags campers={ this.props.campers } tags={ post.tags } />
          <Post.Time time={ post.post_modified } />
          { post.video_url ?
            null
            :
            <Post.Title title={ post.post_title } />
          }
          { post.video_url ?
            null
            :
            <Post.Excerpt excerpt={ post.post_excerpt } />
          }
          </Post.Post>
        ) )
      }
    </View>
    )

    render() {
      const posts = _.sortBy( this.state.posts, post => {
        return -moment( post.post_modified )
      } ).slice( 0, 41 )

      return (
        <Container theme={ theme } style={ styles.backgroundContainer }>
          <Header>
            <HeaderContent />
          </Header>
          <Content
            style={ { marginBottom: ( Platform.OS === "ios" ) ? -50 : -10 } }
            refreshControl={
              <RefreshControl
                refreshing={ this.state.isRefreshing }
                onRefresh={ this.pullDownRefresh }
                tintColor={ '#fff' }
              />
            }
          >
            { this._renderNewsPosts( posts ) }
          </Content>
        </Container>
      )
    }

}

function bindAction( dispatch ) {
  return {
    dispatch,
    openDrawer: () => dispatch( openDrawer() ),
    reset: key => dispatch( reset( [ { key: "login" } ], key, 0 ) ),
    resetAuth: () => dispatch( toggleAllLoginActionsAuth() ),
    pushRoute: ( route, key ) => dispatch( pushRoute( route, key ) ),
  }
}

const mapStateToProps = state => ( {
  navigation: state.cardNavigation,
  campers: state.auth.campers,
  posts: state.news.posts,
} )

export default connect( mapStateToProps, bindAction )( Home )
