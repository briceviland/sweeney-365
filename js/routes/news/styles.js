const React = require( "react-native" )

const { StyleSheet, Dimensions, Platform } = React

const SCREEN = Dimensions.get( "window" )
const COLORS = require( "../../themes/variable" )

module.exports = StyleSheet.create( {
  backgroundContainer: {
    backgroundColor: COLORS.darkBackground,
  },
  newsContainer: {
    margin: 7.5,
  },
  newsHeading: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
  },
  newsHeadingText: {
    fontSize: 21,
    fontWeight: "900",
  },
} )
