const React = require( "react-native" )

const { StyleSheet, Dimensions, Platform } = React

const SCREEN = Dimensions.get( "window" )
const COLORS = require( "../../themes/variable" )

module.exports = StyleSheet.create( {
  backgroundContainer: {
    // backgroundColor: COLORS.darkBackground,
    backgroundColor: "#f1f1f1",
  },
  newsContainer: {

  },
  postContainer: {
    // backgroundColor: '#fff',
    flex: 1,
    flexDirection: "column",
  },
  postImage: {
    // : Set image size based on wp image size
    height: 200,
  },
  postInfo: {
    flexDirection: "column",
    padding: 15,
    flex: 1,
  },
  postTitle: {
    color: "#222",
    fontWeight: "bold",
    fontSize: 18,
    marginBottom: 10,
  },
  postContent: {
    height: 400,
    flex: 1,
    // color: '#222'
  },
} )
