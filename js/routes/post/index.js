import _ from "lodash/core"
import React, { Component } from "react"
import {
  Image,
  WebView,
  Platform,
} from "react-native"

import { connect } from "react-redux"
import { Container, Header, Content, Text, View } from "native-base"
import { Grid, Col, Row } from "react-native-easy-grid"
import HTMLView from "react-native-htmlview"

import { actions } from "react-native-navigation-redux-helpers"

import HeaderContent from "../../components/headerContent/"

import theme from "../../themes/base-theme"
import styles from "./styles"

const {
  popRoute,
  pushRoute,
} = actions

class Post extends Component {

  static propTypes = {
    post: React.PropTypes.object,
    popRoute: React.PropTypes.func,
    pushRoute: React.PropTypes.func,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
    } ),
  }

  popRoute() {
    this.props.popRoute( this.props.navigation.key )
  }

  pushRoute( route, passProps ) {
    this.props.pushRoute( { key: route, index: this.props.navigation.index + 1, passProps }, this.props.navigation.key )
  }

  render() {
    const post = this.props.post

    return (
      <Container theme={ theme } style={ styles.backgroundContainer }>
        <Header>
          <HeaderContent />
        </Header>
        <Content style={ { marginBottom: ( Platform.OS === "ios" ) ? -50 : -10 } }>
          <View style={ styles.postContainer }>
            { post.image &&
              <Image style={ styles.postImage } source={ { uri: post.image } } />
            }
            <View style={ styles.postInfo }>
              <Text style={ styles.postTitle }>
                {post.post_title}
              </Text>
              <HTMLView
                style={ styles.postContent }
                value={ post.post_content }
                onLinkPress={ url => this.pushRoute( "webviewer", { url } ) }
              />
            </View>
          </View>
        </Content>
      </Container>
    )
  } // : render()

}

function bindAction( dispatch ) {
  return {
    popRoute: key => dispatch( popRoute( key ) ),
    pushRoute: ( route, key ) => dispatch( pushRoute( route, key ) ),
  }
}

const mapStateToProps = ( state, ownProps ) => ( {
  navigation: state.cardNavigation,
  post: _.find( state.news.posts, { id: ownProps.post_id } ),
} )

export default connect( mapStateToProps, bindAction )( Post )
