import _ from "lodash/core"
import React, { Component } from "react"
import {
  Image,
  Platform,
  TouchableOpacity,
} from "react-native"

import { connect } from "react-redux"
import { Container, Header, Content, Text, View, Spinner } from "native-base"
import { Grid, Col, Row } from "react-native-easy-grid"

import Icon from "react-native-vector-icons/FontAwesome"

import { actions } from "react-native-navigation-redux-helpers"
import { actGetCabinsInfo } from "../../actions/cabins"

import HeaderContent from "../../components/headerContent/"

import theme from "../../themes/base-theme"
import styles from "./styles"

const {
  popRoute,
  pushRoute,
} = actions

const CARD_COLORS = [ "#01cca1", "#2594f6", "#c83833", "#e86713" ]

class Cabins extends Component {

  static propTypes = {
    cabins: React.PropTypes.array.isRequired,
    popRoute: React.PropTypes.func.isRequired,
    pushRoute: React.PropTypes.func.isRequired,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
      index: React.PropTypes.number,
    } ).isRequired,
  }

  constructor( props ) {
    super( props )
    this.state = {
      // loading: true,
      cabins: props.cabins,
    }
  }

  componentWillMount() {
    this.props.actGetCabinsInfo().then( ( cabins ) => {
      this.setState( { cabins: cabins } )
    } )
  }

  popRoute() {
    this.props.popRoute( this.props.navigation.key )
  }

  pushRoute( route, passProps ) {
    this.props.pushRoute( {
      key: route,
      index: this.props.navigation.index + 1,
      passProps,
    }, this.props.navigation.key )
  }

  _renderCabinCol = ( cabin, index ) => (
    <Col
      key={ cabin.id }
      style={ [
        styles.cabinCard,
        styles.shadowBase,
        { backgroundColor: cabin.color }
      ] }
    >
      <TouchableOpacity style={ styles.cabinCardTouchable } onPress={ () => this.pushRoute( "cabin", { cabin_id: cabin.id } ) }>
        {
          cabin.thumbnail ?
          <Image source={{ uri: cabin.thumbnail }} style={ [ styles.cabinImage, styles.centerContent ] }>
            <Text style={ [ styles.cabinName, styles.textShadow ] }>{ cabin.name }</Text>
          </Image>
          :
          <View style={ styles.centerContent }>
            <Text style={ styles.cabinName }>{ cabin.name }</Text>
          </View>
        }
      </TouchableOpacity>
    </Col>
    )

  _renderCabinRows = ( cabins ) => {
    const content = []
    for ( let i = 0; i < cabins.length; i += 2 ) {
      content.push(
        <Row key={ i }>
          { this._renderCabinCol( cabins[ i ], i ) }
          { i < cabins.length - 1 && this._renderCabinCol( cabins[ i + 1 ], i + 1 ) }
        </Row>,
      )
    }
    return content
  }

  _renderLoading = () => (
    <Spinner color="white" />
  )

  render() {
    const cabins = _.sortBy( this.state.cabins, 'order' )

    return (
      <Container theme={ theme } style={ styles.backgroundContainer }>
        <Header>
          <HeaderContent />
        </Header>
        <Content style={ { marginBottom: ( Platform.OS === "ios" ) ? -50 : -10 } }>
          { cabins.length == 0 && this._renderLoading() }
          <Grid style={ styles.cabinsContainer }>
            { this._renderCabinRows( cabins ) }
          </Grid>
        </Content>
      </Container>
    )
  } // : render()

}

function bindAction( dispatch ) {
  return {
    actGetCabinsInfo: () => dispatch( actGetCabinsInfo() ),
    pushRoute: ( route, key ) => dispatch( pushRoute( route, key ) ),
    popRoute: key => dispatch( popRoute( key ) ),
  }
}

const mapStateToProps = state => ( {
  navigation: state.cardNavigation,
  cabins: state.cabins.cabins,
} )

export default connect( mapStateToProps, bindAction )( Cabins )
