const React = require( "react-native" )

const { StyleSheet, Dimensions, Platform } = React

const SCREEN = Dimensions.get( "window" )
const COLORS = require( "../../themes/variable" )

module.exports = StyleSheet.create( {
  backgroundContainer: {
    backgroundColor: COLORS.darkBackground,
  },
  shadowBase: {
    shadowRadius: 4,
    shadowColor: "#000",
    shadowOpacity: 0.25,
    shadowOffset: {
      width: 0,
      height: 0,
    },
  },
  cabinsContainer: {
    margin: 7.5,
  },
  cabinCard: {
    margin: 7.5,
    flex: 1,
    height: ( SCREEN.width - ( 7.5 * 4 ) ) / 2,
    backgroundColor: COLORS.brandSecondary,
  },
  cabinCardTouchable: {
    flex: 1,
  },
  centerContent: {
    padding: 20,
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  cabinName: {
    backgroundColor: 'transparent',
    fontWeight: "900",
    fontSize: 20,
    lineHeight: 20,
    textAlign: "center",
  },
  textShadow: {
    textShadowColor: '#222',
    textShadowOffset: { width: 1, height: 1 },
    textShadowRadius: 1,
  },
} )
