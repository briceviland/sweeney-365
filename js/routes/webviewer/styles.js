const React = require( "react-native" )

const { StyleSheet, Dimensions, Platform } = React

const SCREEN = Dimensions.get( "window" )
const COLORS = require( "../../themes/variable" )

module.exports = StyleSheet.create( {
  backgroundContainer: {
    backgroundColor: COLORS.darkBackground,
  },
  webViewer: {
    width: SCREEN.width,
    height: SCREEN.height,
  },
  loadingView: {
    height: SCREEN.height - 70,
    width: SCREEN.width,
    justifyContent: "center",
    alignItems: "center",
    zIndex: 0,
  },
} )
