import _ from "lodash/core"
import React, { Component } from "react"
import {
  WebView,
  Platform,
} from "react-native"

import { connect } from "react-redux"
import { Container, Header, Content, View, Text } from "native-base"

import { actions } from "react-native-navigation-redux-helpers"

import HeaderContent from "../../components/headerContent/"

import theme from "../../themes/base-theme"
import styles from "./styles"

const {
  popRoute,
} = actions

class WebViewer extends Component {

  static propTypes = {
    url: React.PropTypes.string,
    popRoute: React.PropTypes.func,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
    } ),
  }

  constructor( props ) {
    super( props )
    this.state = {
      loading: true,
    }
  }

  popRoute() {
    this.props.popRoute( this.props.navigation.key )
  }

  onNavigationStateChange = ( navState ) => {
    this.setState( {
      loading: navState.loading,
    } )
  }

  render() {
    const url = this.props.url
    const loading = this.state.loading

    return (
      <Container theme={ theme } style={ styles.backgroundContainer }>
        <Header>
          <HeaderContent />
        </Header>
        <Content style={ { marginBottom: ( Platform.OS === "ios" ) ? -50 : -10 } } scrollEnabled={ false }>
          { loading &&
            <View style={ styles.loadingView }>
              <Text>Loading...</Text>
            </View>
          }
          <WebView
            style={ styles.webViewer }
            source={ { uri: url } }
            onNavigationStateChange={ this.onNavigationStateChange }
            onShouldStartLoadWithRequest={ () => true }
          />
        </Content>
      </Container>
    )
  } // : render()

}

function bindAction( dispatch ) {
  return {
    popRoute: key => dispatch( popRoute( key ) ),
  }
}

const mapStateToProps = ( state, ownProps ) => ( {
  navigation: state.cardNavigation,
} )

export default connect( mapStateToProps, bindAction )( WebViewer )
