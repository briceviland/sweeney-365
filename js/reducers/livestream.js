import type { Action } from "../actions/types"
import { ADD_LIVE_STREAM_VIDEO, ADD_LIVE_STREAM_CURRENT_VIDEO } from "../actions/livestream"

export type State = {
  videos: Array,
  current_live_video_url: string,
}

const initialState = {
  videos: [],
  current_live_video_url: "",
}

export default function ( state:State = initialState, action:Action ): State {
  let _state = state
  if ( action.type === ADD_LIVE_STREAM_VIDEO ) {
    const _videos = state.videos
    _.remove( _videos, { id: action.video.id } )
    _videos.push( action.video )

    _state = {
      ...state,
      videos: _videos,
    }
  }
  else if ( action.type === ADD_LIVE_STREAM_CURRENT_VIDEO ) {
    _state = {
      ...state,
      current_live_video_url: action.videoUrl
    }
  }
  return _state
}
