import type { Action } from "../actions/types"
import { SET_CURRENT_WEATHER } from "../actions/weather"

export type State = {
  currently: object
}

const initialState = {
  currently: new Object(),
}

export default function ( state:State = initialState, action:Action ): State {
  if ( action.type === SET_CURRENT_WEATHER ) {
    state = {
      ...state,
      currently: action.currently,
    }
  }
  return state
}
