import _ from "lodash"

import type { Action } from "../actions/types"
import { ADD_CABIN_INFO, ADD_CAMPER_OF, RESET_CAMPER_OF } from "../actions/cabins"

export type State = {
  cabins: Array<Object>,
  campersOf: Array<Object>,
}

const initialState = {
  cabins: [],
  campersOf: [],
}

export default function ( state:State = initialState, action:Action ): State {
  let _state = state
  if ( action.type === ADD_CABIN_INFO ) {
    const _cabins = state.cabins
    _.remove( _cabins, { id: action.cabin.id } )
    _cabins.push( action.cabin )

    _state = {
      ...state,
      cabins: _cabins,
    }
  } else if ( action.type === ADD_CAMPER_OF ) {
    const _campersOf = state.campersOf
    _.remove( _campersOf, { key: action.camper.key } )
    _campersOf.push( action.camper )

    _state = {
      ...state,
      campersOf: _campersOf,
    }
  } else if ( action.type === RESET_CAMPER_OF ) {
    _state = {
      ...state,
      campersOf: [],
    }
  }
  return _state
}
