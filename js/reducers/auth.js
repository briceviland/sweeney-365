import type { Action } from "../actions/types"
import { TOGGLE_HAS_SHOWN_SETUP, TOGGLE_LOGIN_AUTH, SET_JTW_TOKEN_AUTH, SET_USER_DATA_AUTH, SET_CAMPER_DATA_AUTH } from "../actions/auth"

export type State = {
  loggedIn: boolean,
  jwtToken: string,
  user_data: Object,
  campers: Array<Object>,
  hasShownSetupScreen: boolean,
}

const initialState = {
  loggedIn: false,
  jwtToken: "",
  user_data: {},
  campers: [],
  hasShownSetupScreen: false,
}

export default function ( state:State = initialState, action:Action ): State {
  if ( action.type === TOGGLE_HAS_SHOWN_SETUP ) {
    state = {
      ...state,
      hasShownSetupScreen: action.hasShownSetupScreen,
    }
  }
  else if ( action.type === TOGGLE_LOGIN_AUTH ) {
    state = {
      ...state,
      loggedIn: action.loggedIn,
    }
  } else if ( action.type === SET_JTW_TOKEN_AUTH ) {
    state = {
      ...state,
      jwtToken: action.jwtToken,
    }
  } else if ( action.type === SET_USER_DATA_AUTH ) {
    state = {
      ...state,
      user_data: action.user_data,
    }
  } else if ( action.type === SET_CAMPER_DATA_AUTH ) {
    const campers = state.campers
    if ( action.camper === null) {
      campers.length = 0
    }
    else {
      _.remove( campers, { id: action.camper.id } )
      campers.push( action.camper )
    }

    state = {
      ...state,
      campers,
    }
  }
  return state
}
