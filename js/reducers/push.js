import type { Action } from "../actions/types"
import { SET_NOTIFICATION_CHANNELS, SET_DEVICE_TOKEN, POSTS_NEED_REFRESH } from "../actions/push"

export type State = {
  channels: Array<string>,
  deviceToken: string,
  postsNeedRefresh: bool,
}

const initialState = {
  channels: [],
  deviceToken: '',
  postsNeedRefresh: false,
}

export default function ( state:State = initialState, action:Action ): State {
  if ( action.type === SET_NOTIFICATION_CHANNELS ) {
    state = {
      ...state,
      channels: action.channels,
    }
  }
  else if ( action.type === SET_DEVICE_TOKEN ) {
    state = {
      ...state,
      deviceToken: action.token,
    }
  }
  else if ( action.type === POSTS_NEED_REFRESH ) {
    state = {
      ...state,
      postsNeedRefresh: action.postsNeedRefresh,
    }
  }
  return state
}
