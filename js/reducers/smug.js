import _ from "lodash"

import type { Action } from "../actions/types"
import { ADD_ALBUM_SMUG } from "../actions/smug"

export type State = {
  albums: Array<Object>
}

const initialState = {
  albums: [],
}

export default function ( state:State = initialState, action:Action ): State {
  if ( action.type === ADD_ALBUM_SMUG ) {
    const albums = state.albums
    _.remove( albums, { key: action.album.key } )
    albums.push( action.album )

    state = {
      ...state,
      albums,
    }
  }
  return state
}
