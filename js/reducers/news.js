import type { Action } from "../actions/types"
import { ADD_NEWS_FEED_POST, RESET_NEWS_FEED } from "../actions/news"

export type State = {
  posts: []
}

const initialState = {
  posts: [],
}

export default function ( state:State = initialState, action:Action ): State {
  if ( action.type === ADD_NEWS_FEED_POST ) {
    const _posts = state.posts
    _.remove( _posts, { id: action.post.id } )
    _posts.push( action.post )

    state = {
      ...state,
      posts: _posts,
    }
  }
  else if ( action.type === RESET_NEWS_FEED ) {
    const _posts = []

    state = {
      ...state,
      posts: _posts,
    }
  }
  return state
}
