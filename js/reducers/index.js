import { combineReducers } from "redux"

import auth from "./auth"
import smug from "./smug"
import livestream from "./livestream"
import news from "./news"
import cabins from "./cabins"
import weather from "./weather"
import drawer from "./drawer"
import cardNavigation from "./cardNavigation"
import push from "./push"

export default combineReducers( {
  auth,
  smug,
  livestream,
  news,
  cabins,
  weather,
  drawer,
  cardNavigation,
  push,
} )
