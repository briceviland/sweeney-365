import React, { Component } from "react"
import codePush from "react-native-code-push"

import AppNavigator from "./AppNavigator"

class App extends Component {
  render() {
    return <AppNavigator />
  }
}

export default codePush( App )
