const color = require( "color" )

// theme background
const primary = color( "#01cca1" )
// header
const secondary = color( "#07589e" )
const info = color( "#2594f6" )
const success = color( "#3bc251" )
const danger = color( "#c83833" )
const warning = color( "#e86713" )
const sidebar = color( "#252932" )
const dark = color( "rgba(0,0,0,0.8)" )
const light = color( "rgba(255,255,255,0.8)" )
const darkBackground = color( "#2679ac" )

module.exports = {
  brandPrimary: primary.hexString().toString(),
  brandSecondary: secondary.hexString().toString(),
  brandInfo: info.hexString().toString(),
  brandSuccess: success.hexString().toString(),
  brandDanger: danger.hexString().toString(),
  brandWarning: warning.hexString().toString(),
  brandSidebar: sidebar.hexString().toString(),
  // darker: darken,
  dark: dark.hexString().toString(),
  light: light.hexString().toString(),
  darkBackground: darkBackground.hexString().toString(),
}
