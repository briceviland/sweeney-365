import React, { Component } from "react"
import { AppState } from 'react-native';
import { Provider } from "react-redux"
import Toast from 'react-native-root-toast'
import { actions } from "react-native-navigation-redux-helpers"

import App from "./App"
import configureStore from "./configureStore"
import SplashPage from "./routes/splashscreen/"
import { toggleAllLoginActionsAuth, actValidateJWTTokenAuth } from "./actions/auth"

import theme from "./themes/base-theme"

const {
  reset,
} = actions

function setup():React.Component {
  class Root extends Component {

    state = {
      appState: AppState.currentState
    }

    componentDidMount() {
      AppState.addEventListener('change', this._handleAppStateChange);
    }

    componentWillUnmount() {
      AppState.removeEventListener('change', this._handleAppStateChange);
    }

    _handleAppStateChange = (nextAppState) => {
      if (nextAppState === 'active' && this.state.isLoading === false) {
        const key = this.state.store.getState().cardNavigation.key
          this.state.store.dispatch( reset( [ { key: "home" } ], key, 0 ) )
      }
      this.setState({appState: nextAppState});
    }

    constructor() {
      super()
      this.state = {
        isLoading: true,
        store: configureStore( () => {
          this.state.store.dispatch( actValidateJWTTokenAuth() ).then( ( isValid ) => {
            if ( ! isValid && this.state.store.getState().auth.hasShownSetupScreen === true ) {
              Toast.show( "You have been logged out.", {
                position: theme.statusBarHeight + 20,
              })
              const key = this.state.store.getState().cardNavigation.key
              this.state.store.dispatch( reset( [ { key: "login" } ], key, 0 ) )
              this.state.store.dispatch( toggleAllLoginActionsAuth() )
            }
            this.setState( { isLoading: false } )
          } )
        } ),
      }
    }

    render() {
      if ( this.state.isLoading ) {
        return <SplashPage />
      }
      return (
        <Provider store={ this.state.store }>
          <App />
        </Provider>
      )
    }
  }
  return Root
}

export default setup
