import _ from "lodash/core"
import React, { Component } from "react"
import {
  BackAndroid,
  StatusBar,
  NavigationExperimental,
  Platform
} from "react-native"
import { connect } from "react-redux"
import { Drawer } from "native-base"
import { actions } from "react-native-navigation-redux-helpers"

import { closeDrawer } from "./actions/drawer"
import { toggleAllLoginActionsAuth } from "./actions/auth"

import AppNotifier from "./AppNotifier"

import SplashPage from "./routes/splashscreen/"
import Login from "./routes/login/"
import SetupScreen from "./routes/setupscreen/"
import Profile from "./routes/profile/"
import InviteUser from "./routes/inviteuser/"
import LiveStream from "./routes/livestream/"
import SmugMug from "./routes/smugmug/"
import PhotoAlbum from "./routes/photoalbum/"
import Home from "./routes/home/"
import News from "./routes/news/"
import Post from "./routes/post/"
import WebViewer from "./routes/webviewer/"
import Cabins from "./routes/cabins/"
import Cabin from "./routes/cabin/"
import Staff from "./routes/staff/"
import Gallery from "./routes/gallery/"

import SideBar from "./components/sideBar"
import NeedHelp from "./routes/needhelp"
import { statusBarColor } from "./themes/base-theme"

var MessageBarAlert = require('react-native-message-bar').MessageBar
var MessageBarManager = require('react-native-message-bar').MessageBarManager

var Orientation = require('react-native-orientation')

const {
  popRoute,
  reset,
} = actions

const {
  CardStack: NavigationCardStack,
} = NavigationExperimental

class AppNavigator extends Component {

  static propTypes = {
    hasShownSetupScreen: React.PropTypes.bool,
    loggedIn: React.PropTypes.bool,
    drawerState: React.PropTypes.string,
    popRoute: React.PropTypes.func,
    reset: React.PropTypes.func,
    resetAuth: React.PropTypes.func,
    closeDrawer: React.PropTypes.func,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
      routes: React.PropTypes.array,
    } ),
  }

  componentDidMount() {

    Orientation.lockToPortrait()

    MessageBarManager.registerMessageBar( this._alert )

    BackAndroid.addEventListener( "hardwareBackPress", () => {
      const routes = this.props.navigation.routes

      if ( routes[ routes.length - 1 ].key === "home" || routes[ routes.length - 1 ].key === "login" ) {
        return false
      }

      this.props.popRoute( this.props.navigation.key )
      return true
    } )

    if ( !this.props.loggedIn ) {
      // this.props.reset( [{ key: 'login', index: 0 }], this.props.navigation.key, 0)
      // this.props.resetAuth()
    }
  }

  componentDidUpdate() {

    if ( this.props.drawerState === "opened" ) {
      this.openDrawer()
    }

    if ( this.props.drawerState === "closed" ) {
      this._drawer.close()
    }
  }

  componentWillUnmount() {
    MessageBarManager.unregisterMessageBar()
  }

  openDrawer() {
    this._drawer.open()
  }

  closeDrawer() {
    if ( this.props.drawerState === "opened" ) {
      this.props.closeDrawer()
    }
  }

  _renderScene( props ) { // eslint-disable-line class-methods-use-this
    switch ( props.scene.route.key ) {
    case "splashscreen":
      return <SplashPage { ...props.scene.route.passProps } />
    case "setupscreen":
      return <SetupScreen { ...props.scene.route.passProps } />
    case "login":
      return <Login { ...props.scene.route.passProps } />
    case "livestream":
      return <LiveStream { ...props.scene.route.passProps } />
    case "smugmug":
      return <SmugMug { ...props.scene.route.passProps } />
    case "photoalbum":
      return <PhotoAlbum { ...props.scene.route.passProps } />
    case "profile":
      return <Profile { ...props.scene.route.passProps } />
    case "inviteuser":
      return <InviteUser { ...props.scene.route.passProps } />
    case "home":
      return <Home { ...props.scene.route.passProps } />
    case "news":
      return <News { ...props.scene.route.passProps } />
    case "post":
      return <Post { ...props.scene.route.passProps } />
    case "webviewer":
      return <WebViewer { ...props.scene.route.passProps } />
    case "cabins":
      return <Cabins { ...props.scene.route.passProps } />
    case "cabin":
      return <Cabin { ...props.scene.route.passProps } />
    case "staff":
      return <Staff { ...props.scene.route.passProps } />
    case "gallery":
      return <Gallery { ...props.scene.route.passProps } />
    case "sideBar":
      return <SideBar />
    case "needhelp":
      return <NeedHelp />

    default:
      return <Home { ...props.scene.route.passProps } />
    }
  }

  render() {  // eslint-disable-line class-methods-use-this
    return (
      <Drawer
        ref={ ( ref ) => { this._drawer = ref } }
        type="overlay"
        tweenDuration={ 150 }
        content={ <SideBar navigator={ this._navigator } /> }
        tapToClose
        acceptPan={ false }
        onClose={ () => this.closeDrawer() }
        openDrawerOffset={ 0.2 }
        panCloseMask={ 0.2 }
        tweenHandler={(ratio) => {  //eslint-disable-line
          return {
            drawer: { shadowRadius: ratio < 0.2 ? ratio * 5 * 5 : 5 },
            main: {
              opacity: ( 2 - ratio ) / 2,
            },
          }
        } }
        negotiatePan
      >
        <StatusBar
          backgroundColor={ statusBarColor }
          barStyle="light-content"
        />
        <NavigationCardStack
          navigationState={ this.props.navigation }
          renderOverlay={ this._renderOverlay }
          renderScene={ this._renderScene }
        />
        <AppNotifier />
        <MessageBarAlert ref={ ( ref ) => { this._alert = ref } } />
      </Drawer>
    )
  }
}

function bindAction( dispatch ) {
  return {
    closeDrawer: () => dispatch( closeDrawer() ),
    reset: ( props, key, index ) => dispatch( reset( props, key, index ) ),
    resetAuth: () => dispatch( toggleAllLoginActionsAuth() ),
    popRoute: key => dispatch( popRoute( key ) ),
  }
}

const mapStateToProps = state => ( {
  hasShownSetupScreen: state.auth.hasShownSetupScreen,
  loggedIn: state.auth.loggedIn,
  drawerState: state.drawer.drawerState,
  navigation: state.cardNavigation,
} )

export default connect( mapStateToProps, bindAction )( AppNavigator )
