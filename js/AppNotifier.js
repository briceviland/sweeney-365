import React, { Component } from "react"
import {
  Platform,
  AppState,
  PushNotificationIOS,
} from "react-native"
import { connect } from "react-redux"
import { actions } from "react-native-navigation-redux-helpers"

import { View, Text } from "native-base"

import Pusher from 'pusher-js/react-native'

import navigateTo from "./actions/sideBarNav"
import { setDeviceToken, registerTokenForUser, actPostsNeedRefresh } from "./actions/push"
import { actSetLiveStreamCurrentVideo } from "./actions/livestream"

var MessageBarManager = require('react-native-message-bar').MessageBarManager

const COLORS = require( "./themes/variable" )

const {
  pushRoute,
} = actions

let pusher = null
let pusherChannel = null

const PUSHER_API_KEY = "2a3577bc69494a38cef3"

class Notifier extends Component {

  static propTypes = {
    channels: React.PropTypes.array,
    setDeviceToken: React.PropTypes.func.isRequired,
    registerTokenForUser: React.PropTypes.func.isRequired,
    pushRoute: React.PropTypes.func.isRequired,
    navigateTo: React.PropTypes.func.isRequired,
    navigation: React.PropTypes.shape( {
      key: React.PropTypes.string,
    } ).isRequired,
  }

  constructor( props ) {
    super( props )

    this.state = {
      isPusherInitialized: false,
    }

  }

  componentWillMount() {
    if ( Platform.OS === "ios" ) {
      PushNotificationIOS.addEventListener( "register", this._pushRegisterEvent )
      PushNotificationIOS.addEventListener( "notification", this._pushNotificationEvent )
    }
    else if ( Platform.OS === "android" ) {

    }
  }

  componentWillUnmount() {
    PushNotificationIOS.removeEventListener( "register", this._pushRegisterEvent )
    PushNotificationIOS.removeEventListener( "registrationError", this._pushRegistrationErrorEvent )
    PushNotificationIOS.removeEventListener( "notification", this._pushNotificationEvent )
  }

  pushRoute( route ) {
    this.props.pushRoute( {
      key: route,
      index: this.props.navigation.index + 1,
    }, this.props.navigation.key )
  }

  navigateTo( route, passProps ) {
    this.props.navigateTo( route, "home", passProps )
  }

  _pushRegisterEvent = ( deviceToken ) => {
    this.props.setDeviceToken( deviceToken )
    this.props.registerTokenForUser( deviceToken )
    console.log( "Device token registered - ", deviceToken )
  }

  _pushRegistrationErrorEvent = (message, code, details) => {
    console.log( "Error registering device - ", message, code, details )
  }

  _pushNotificationEvent = ( notification ) => {

    if ( notification._alert.type === 'livestream' && notification._alert.livestream_url !== '' ) {
      this.props.setLiveStreamUrl( notification._alert.livestream_url )
    }

    if ( AppState.currentState === 'background' ) {
      if ( notification._alert.type === 'livestream' && _.last(this.props.navigation.routes).key !== "livestream" ) {
        this.navigateTo( "livestream" )
      }
      else if ( notification._alert.type === 'tag' ) {
        this.props.actPostsNeedRefresh( true )
        this.navigateTo( "home" )
      }
    }
    else if ( AppState.currentState === 'active' ) {
      console.log("Show in-App alert!")
      const title = notification._alert.title
      const body = notification._alert.body

      let alertArgs = {
        title:    title,
        message:  body,
        alertType: 'info',
        position: 'bottom',
        duration: 7000,
        animationType: 'SlideFromLeft',
        messageNumberOfLines: 4,
        titleStyle: { color: 'white', fontSize: 18, fontWeight: 'bold', marginBottom: 4 },
        stylesheetInfo : { backgroundColor: COLORS.brandSecondary, strokeColor: COLORS.brandSecondary  },
      }

      if ( notification._alert.type === 'livestream' && _.last(this.props.navigation.routes).key !== "livestream" ) {
        alertArgs['onTapped'] = () => { this.navigateTo( "livestream" ) }
      }
      else if ( notification._alert.type === 'tag' ) {

        alertArgs['onTapped'] = () => {
          this.props.actPostsNeedRefresh( true )
          this.navigateTo( "home" )
        }
      }

      MessageBarManager.showAlert( alertArgs )
    }

  }

  initPusher() {
    // If debug
    Pusher.logToConsole = true
    pusher = new Pusher( PUSHER_API_KEY, {
      encrypted: true
    } )

    pusherChannel = pusher.subscribe( "sweeney" )

    _.each( this.props.channels, ( name ) => {

      if ( name === "tags" ) {
        _.each( this.props.campers, (camper) => {
          const camperTag = `tag_${camper.id}`
          pusherChannel.bind( camperTag, ( data ) => {
            console.log("WEBSOCKET TAG Push", data)
          } )
        } )
      }
      else {
        pusherChannel.bind( name, ( data ) => {
          console.log("WEBSOCKET Push", data)
        } )
      }

    } )

    this.setState({
      isPusherInitialized: true,
    })
    console.log("INIT PUSHER")
  }

  render() {
    // const shouldInitPusher =  false === this.state.isPusherInitialized &&
    //                           true === this.state.loggedIn &&
    //                           true === this.state.hasShownSetupScreen
    //
    //
    // if ( shouldInitPusher === true ) {
    //   this.initPusher()
    // }
    return null
  }

}

function bindAction( dispatch ) {
  return {
    actPostsNeedRefresh: ( needRefresh ) => dispatch( actPostsNeedRefresh( needRefresh ) ),
    setLiveStreamUrl: ( videoUrl ) => dispatch( actSetLiveStreamCurrentVideo( videoUrl ) ),
    registerTokenForUser: ( deviceToken ) => dispatch( registerTokenForUser( deviceToken ) ),
    setDeviceToken: ( deviceToken ) => dispatch( setDeviceToken( deviceToken ) ),
    pushRoute: ( route, key ) => dispatch( pushRoute( route, key ) ),
    navigateTo: ( route, homeRoute ) => dispatch( navigateTo( route, homeRoute ) ),
  }
}

const mapStateToProps = state => ( {
  campers: state.auth.campers,
  channels: state.push.channels,
  navigation: state.cardNavigation,
} )

export default connect( mapStateToProps, bindAction )( Notifier )
