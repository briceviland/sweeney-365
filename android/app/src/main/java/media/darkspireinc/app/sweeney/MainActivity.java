package media.darkspireinc.app.sweeney;

import com.facebook.react.ReactActivity;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "Sweeney365";
    }

    // @Override
    // protected void onCreate(Bundle savedInstanceState) {
    //     PusherAndroid pusher = new PusherAndroid("2a3577bc69494a38cef3");
    //     PushNotificationRegistration nativePusher = pusher.nativePusher();
    //     nativePusher.registerFCM(this);
    //
    //     nativePusher.subscribe("messages");
    //     nativePusher.subscribe("livestream");
    //
    //
    // }

}
